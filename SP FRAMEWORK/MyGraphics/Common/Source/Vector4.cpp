#include "Vector4.h"

Vector4::Vector4(float a, float b, float c, float d)
{
	x = a;
	y = b;
	z = c;
	w = d;
}
Vector4::Vector4(const Vector4 &rhs)
{
	w = rhs.w;
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
}
Vector4::~Vector4()
{

}

void Vector4::Set(float a, float b, float c, float d)
{
	x = a;
	y = b;
	z = c;
	w = d;
}
void Vector4::SetZero(void)
{
	w = 1;
	x = 0;
	y = 0;
	z = 0;
}
bool Vector4::IsZero(void) const
{
	return ((w == 0) && (x == 0) && (y == 0) && (z == 0));
}