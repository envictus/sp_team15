#ifndef VECTOR_4_H
#define VECTOR_4_H

#include "MyMath.h"
#include <iostream>

#pragma warning( disable: 4290 ) //for throw(DivideByZero)

struct Vector4
{
	float w, x, y, z;
	Vector4(float a = 0.0, float b = 0.0, float c = 0.0, float d = 0.0);
	Vector4(const Vector4 &rhs);
	~Vector4();

	void Set(float a = 0, float b = 0, float c = 0, float d = 0); //Set all data
	void SetZero(void); //Set all data to zero
	bool IsZero(void) const; //Check if data is zero
};

#endif // !VECTOR_4_H#pragma once