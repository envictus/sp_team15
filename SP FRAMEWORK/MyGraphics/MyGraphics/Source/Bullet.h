#ifndef BULLET_H
#define BULLET_H
#include <vector>
#include "Vector3.h"
#include "Shooting.h"

class Projectile
{
public:
	Projectile();
	~Projectile();
	Shooting Shooting;
};

#endif