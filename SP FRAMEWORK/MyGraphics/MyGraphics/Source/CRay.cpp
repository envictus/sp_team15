#include "CRay.h"

CRay::CRay()
{
	this->position = Vector3(0, 0, 0);
	this->direction = Vector3(0, 0, -1);
}

CRay::CRay(Vector3 p, Vector3 d)
{
	this->position = p;
	this->direction = d;
}

void CRay::SetPosition(Vector3 p)
{
	this->position = p;
}

Vector3 CRay::GetPosition(void)
{
	return this->position;
}

void CRay::SetDirection(Vector3 d)
{
	this->direction = d;
}

Vector3 CRay::PointOnRay(double scalar)
{
	return (position + (direction * scalar));
}
