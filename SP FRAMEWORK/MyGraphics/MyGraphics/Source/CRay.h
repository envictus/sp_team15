#pragma once
#include "Vector3.h"
class CRay
{
private:
	Vector3 position;
	Vector3 direction;
public:
	CRay();
	CRay(Vector3, Vector3);
	void SetPosition(Vector3);
	Vector3 GetPosition(void);
	void SetDirection(Vector3);
	Vector3 PointOnRay(double scalar);
};