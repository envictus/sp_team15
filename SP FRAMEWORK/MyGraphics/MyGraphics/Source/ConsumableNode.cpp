#include "ConsumableNode.h"
#include <iostream>
#include <string>

using namespace std;

ConsumableNode::ConsumableNode()
{
	data = 0;
	hpGain = 0;
}

ConsumableNode::ConsumableNode(int d, int h)
{
	data = d;
	hpGain = h;
	cNext = NULL;
	cPrev = NULL;
}

ConsumableNode::~ConsumableNode()
{
}

int ConsumableNode::getData()
{
	return data;
}

void ConsumableNode::setData(int d)
{
	data = d;
}

int ConsumableNode::getHPGain()
{
	return hpGain;
}

void ConsumableNode::setHPGain(int h)
{
	hpGain = h;
}
