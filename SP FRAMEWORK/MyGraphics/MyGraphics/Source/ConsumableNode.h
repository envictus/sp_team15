#pragma once
class ConsumableNode
{
private:
	int data;
	int hpGain;
public:
	ConsumableNode();
	ConsumableNode(int, int);
	~ConsumableNode();
	ConsumableNode* cNext;
	ConsumableNode* cPrev;

	int getData();
	void setData(int);

	int getHPGain();
	void setHPGain(int);
};

