#include "SP_SCENE_1.h"
#include "SP_SCENE_2.h"


using namespace std;

void SceneText::EnemyMoving(double dt)
{
	if ((enemy.x - camera.position.x) + (enemy.z - camera.position.z) <= 5)
	{
		enemy.x -= (float)((enemy.x - camera.position.x) * dt / 1);
		enemy.z -= (float)((enemy.z - camera.position.z) * dt / 1);

	}
}
void SceneText::EnemyMoving2(double dt)
{
	if ((enemy2.x - camera.position.x) + (enemy2.z - camera.position.z) <= 5)
	{
		enemy2.x -= (float)((enemy2.x - camera.position.x) * dt / 1);
		enemy2.z -= (float)((enemy2.z - camera.position.z) * dt / 1);

	}
}

void SceneText::EnemyMoving3(double dt)
{
	if ((enemy3.x - camera.position.x) + (enemy3.z - camera.position.z) <= 5)
	{
		enemy3.x -= (float)((enemy3.x - camera.position.x) * dt / 1);
		enemy3.z -= (float)((enemy3.z - camera.position.z) * dt / 1);

	}
}

void SceneText::EnemyMoving4(double dt)
{
	if ((enemy4.x - camera.position.x) + (enemy4.z - camera.position.z) <= 5)
	{
		enemy4.x -= (float)((enemy4.x - camera.position.x) * dt / 2);
		enemy4.z -= (float)((enemy4.z - camera.position.z) * dt / 2);

	}
}


void SceneText::EnemyMoving5(double dt)
{
	if ((enemy5.x - camera.position.x) + (enemy5.z - camera.position.z) <= 5)
	{
		enemy5.x -= (float)((enemy5.x - camera.position.x) * dt / 2);
		enemy5.z -= (float)((enemy5.z - camera.position.z) * dt / 2);

	}
}



void SceneText::EnemyHealth()
{
	if ((enemy.x - camera.position.x) + (enemy.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime < 50)
		{
			elapsedtime++;
		}
		else if (elapsedtime >= 50)
		{
			health = health - 3;
			elapsedtime = 0;
		}
	}
}
void SceneText::EnemyHealth2()
{
	if ((enemy2.x - camera.position.x) + (enemy2.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime2 < 50)
		{
			elapsedtime2++;
		}
		else if (elapsedtime2 >= 50)
		{
			health = health - 3;
			elapsedtime2 = 0;
		}
	}
}
void SceneText::EnemyHealth3()
{
	if ((enemy3.x - camera.position.x) + (enemy3.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime3 < 50)
		{
			elapsedtime3++;
		}
		else if (elapsedtime3 >= 50)
		{
			health = health - 3;
			elapsedtime3 = 0;
		}
	}
}

void SceneText::EnemyHealth4()
{
	if ((enemy4.x - camera.position.x) + (enemy4.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime4 < 50)
		{
			elapsedtime4++;
		}
		else if (elapsedtime4 >= 50)
		{
			health = health - 3;
			elapsedtime4 = 0;
		}
	}
}

void SceneText::EnemyHealth5()
{
	if ((enemy5.x - camera.position.x) + (enemy5.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime5 < 50)
		{
			elapsedtime5++;
		}
		else if (elapsedtime5 >= 50)
		{
			health = health - 3;
			elapsedtime5 = 0;
		}
	}
}




void Scene_2::EnemyMoving(double dt)
{
	if ((enemy.x - camera.position.x) + (enemy.z - camera.position.z) <= 5)
	{
		enemy.x -= (float)((enemy.x - camera.position.x) * dt / 1);
		enemy.z -= (float)((enemy.z - camera.position.z) * dt / 1);

	}
}


void Scene_2::EnemyMoving2(double dt)
{
	if ((enemy2.x - camera.position.x) + (enemy2.z - camera.position.z) <= 5)
	{
		enemy2.x -= (float)((enemy2.x - camera.position.x) * dt / 2);
		enemy2.z -= (float)((enemy2.z - camera.position.z) * dt / 2);

	}
}

void Scene_2::EnemyMoving3(double dt)
{
	if ((enemy3.x - camera.position.x) + (enemy3.z - camera.position.z) <= 5)
	{
		enemy3.x -= (float)((enemy3.x - camera.position.x) * dt / 1);
		enemy3.z -= (float)((enemy3.z - camera.position.z) * dt / 1);

	}
}

void Scene_2::EnemyMoving4(double dt)
{
	if ((enemy4.x - camera.position.x) + (enemy4.z - camera.position.z) <= 5)
	{
		enemy4.x -= (float)((enemy4.x - camera.position.x) * dt / 1);
		enemy4.z -= (float)((enemy4.z - camera.position.z) * dt / 1);

	}
}

void Scene_2::EnemyMoving5(double dt)
{
	if ((enemy5.x - camera.position.x) + (enemy5.z - camera.position.z) <= 5)
	{
		enemy5.x -= (float)((enemy5.x - camera.position.x) * dt / 2);
		enemy5.z -= (float)((enemy5.z - camera.position.z) * dt / 2);

	}
}




void Scene_2::EnemyHealth()
{
	if ((enemy.x - camera.position.x) + (enemy.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime < 50)
		{
			elapsedtime++;
		}
		else if (elapsedtime >= 50)
		{
			health = health - 3;
			elapsedtime = 0;
		}
	}
}

void Scene_2::EnemyHealth2()
{
	if ((enemy2.x - camera.position.x) + (enemy2.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime2 < 50)
		{
			elapsedtime2++;
		}
		else if (elapsedtime2 >= 50)
		{
			health = health - 3;
			elapsedtime2 = 0;
		}
	}
}

void Scene_2::EnemyHealth3()
{
	if ((enemy3.x - camera.position.x) + (enemy3.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime3 < 50)
		{
			elapsedtime3++;
		}
		else if (elapsedtime3 >= 50)
		{
			health = health - 3;
			elapsedtime3 = 0;
		}
	}
}

void Scene_2::EnemyHealth4()
{
	if ((enemy4.x - camera.position.x) + (enemy4.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime4 < 50)
		{
			elapsedtime4++;
		}
		else if (elapsedtime4 >= 50)
		{
			health = health - 3;
			elapsedtime4 = 0;
		}
	}
}

void Scene_2::EnemyHealth5()
{
	if ((enemy5.x - camera.position.x) + (enemy5.z - camera.position.z) <= 5 && health >= 1)
	{
		if (elapsedtime5 < 50)
		{
			elapsedtime5++;
		}
		else if (elapsedtime5 >= 50)
		{
			health = health - 3;
			elapsedtime5 = 0;
		}
	}
}
