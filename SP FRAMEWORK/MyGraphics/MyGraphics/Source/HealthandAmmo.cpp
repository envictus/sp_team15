#include "SP_SCENE_1.h"

using namespace std;


void SceneText::Healthkit()
{
	cout << "time" << elapsedtime << endl;
	Box healthstand = Box(Vector3(-24, 0, 30), 2);
	bool collided_healthstand = healthstand.isPointInBox(camera.position, healthstand);

	Box healthstand2 = Box(Vector3(-24, 0, 30), 2.5);
	bool collided_healthstand2 = healthstand2.isPointInBox(camera.position, healthstand2);
	if (gamestate != GAME_4 && collided_healthstand)
	{
		switch (gamestate)
		{
		case GAME_1:
			healthhealed = rand() % 40 + 10;
			if (elapsedtime < 50)
			{
				if (Application::IsKeyPressed('E'))
				{
					RenderTextOnScreen(meshList[GEO_TEXT], "You are using health kit", Color(1, 1, 1), 5, 6, 6), false;
					elapsedtime++;
				}
				else
				{
					elapsedtime = 0;
				}
			}
			else if (elapsedtime >= 50)
			{
				if ((health + healthhealed) < 100)
				{
					health = health + healthhealed;
					elapsedtime = 0;
					gamestate = GAME_2;
				}
				else
				{
					health = 100;
					elapsedtime = 0;
					gamestate = GAME_2;
				}
			}
			break;
			/*if (Application::IsKeyPressed('E'))
			{
			if ((elapsedtime < 50) && (Application::IsKeyPressed('E')))
			{
			RenderTextOnScreen(meshList[GEO_TEXT], "You are using health kit", Color(1, 1, 1), 5, 6, 6), false;
			elapsedtime++;
			}
			else if ((elapsedtime < 50) && (Application::IsKeyPressed('E')))
			{
			elapsedtime = 0;
			}
			else if (elapsedtime >= 50)
			{
			if ((health + healthhealed) < 100)
			{
			health = health + healthhealed;
			elapsedtime = 0;
			gamestate = GAME_2;
			}
			else
			{
			health = 100;
			elapsedtime = 0;
			gamestate = GAME_2;
			}
			}
			}*/
		case GAME_2:
			healthhealed = rand() % 30 + 10;
			if (Application::IsKeyPressed('E'))
			{
				if (elapsedtime < 50)
				{
					RenderTextOnScreen(meshList[GEO_TEXT], "You are healing", Color(1, 1, 1), 5, 6, 6), false;
					elapsedtime++;
				}
				else if (elapsedtime >= 50)
				{
					if ((health + healthhealed) < 100)
					{
						health = health + healthhealed;
						elapsedtime = 0;
						gamestate = GAME_3;
					}
					else
					{
						health = 100;
						elapsedtime = 0;
						gamestate = GAME_3;
					}
				}
			}
			break;

		case GAME_3:
			std::cout << "You already healed" << endl;
			//RenderTGAOnScreen(meshList[GEO_OMAEWAMOSHINDEIRU], Color(1, 1, 1), 5, 8, 6), false;
			break;

		case GAME_4:
			break;

		default:
			break;
		}
	}
	if (gamestate != (GAME_1 && GAME_3 && GAME_4) && collided_healthstand2 && !collided_healthstand)
	{
		gamestate = GAME_3;
	}
}




void SceneText::Bread()
{
	Box breadlocation = Box(Vector3(-2.5, 0, -2.85), 3);
	bool collided_breadlocation = breadlocation.isPointInBox(camera.position, breadlocation);
	if (collided_breadlocation)
	{
		cout << "bread" << endl;
		if (Application::IsKeyPressed('E') && (render_bread == true))
		{
			render_bread = false;
			//RenderTextOnScreen(meshList[GEO_TEXT], "You have picked up bread", Color(1, 1, 1), 5, 5, 5), false;
			bread++;
		}
	}

	if ((bread >= 1) && (Application::IsKeyPressed('T')))
	{
		breadhealed = rand() % 20 + 1;
		if (elapsedtime < 10)
		{
			elapsedtime++;
		}
		else if (elapsedtime >= 10)
		{
			if ((health + breadhealed) < 100)
			{
				health = health + breadhealed;
				bread--;
			}
			else
			{
				health = 100;
				bread--;
			}
			elapsedtime = 0;
		}
	}

}

void SceneText::magazine()
{
	Box breadlocation = Box(Vector3(-15, 0, 5), 2);
	bool collided_breadlocation = breadlocation.isPointInBox(camera.position, breadlocation);
	if (collided_breadlocation)
	{
		cout << "bread" << endl;
		if (Application::IsKeyPressed('Y') && (render_magazine == true))
		{
			render_magazine = false;
			//RenderTextOnScreen(meshList[GEO_TEXT], "You have picked up bread", Color(1, 1, 1), 5, 5, 5), false;
			ammo += 10;
		}
	}

}