#include "SP_SCENE_1.h"
#include "SP_SCENE_2.h"
#include "Application.h"
#include "Box.h"
#include "object.h"

using namespace std;

void SceneText::DoorCheck()
{
	Box door = Box(Vector3(-6.2, 0, 21.4), 3);
	collided_door1 = door.isPointInBox(camera.position, door);
	if (collided_door1)
	{
		cout << "OWO" << endl;
		if ((door1_open == false) && (carded = true))
		{
			if (Application::IsKeyPressed('E'))
			{
				carded = false;
				door1_open = true;
			}
		}
	}

	Box door2 = Box(Vector3(22, 0, 32), 3);
	collided_door2 = door2.isPointInBox(camera.position, door2);
	if (collided_door2)
	{
		cout << "OWO" << endl;
		if ((door2_open == false) && (carded = true))
		{
			if (Application::IsKeyPressed('E'))
			{
				carded = false;
				door2_open = true;
			}
		}
	}


}

void SceneText::CardCheck()
{
	Box card1 = Box(Vector3(22, 0.6, -31), 2);
	collided_card1 = card1.isPointInBox(camera.position, card1);
	if (collided_card1)
	{
		if (Application::IsKeyPressed('E'))
		{
			render_card1 = false;
			carded = true;
		}
	}

	Box card2 = Box(Vector3(22, 0.6, -29), 2);
	collided_card2 = card2.isPointInBox(camera.position, card2);
	if (collided_card2)
	{
		if (Application::IsKeyPressed('E'))
		{
			render_card2 = false;
			carded = true;
		}
	}
}
void Scene_2::DoorCheck()
{
	Box door = Box(Vector3(-6.2, 0, 21.4), 3);
	collided_door1 = door.isPointInBox(camera.position, door);
	if (collided_door1)
	{
		cout << "OWO" << endl;
		if ((door1_open == false) && (carded = true))
		{
			if (Application::IsKeyPressed('E'))
			{
				carded = false;
				door1_open = true;
			}
		}
	}

	Box door2 = Box(Vector3(22, 0, 32), 3);
	collided_door2 = door2.isPointInBox(camera.position, door2);
	if (collided_door2)
	{
		cout << "OWO" << endl;
		if ((door2_open == false) && (carded = true))
		{
			if (Application::IsKeyPressed('E'))
			{
				carded = false;
				door2_open = true;
			}
		}
	}


}

void Scene_2::CardCheck()
{
	Box card1 = Box(Vector3(22, 0.6, -31), 2);
	collided_card1 = card1.isPointInBox(camera.position, card1);
	if (collided_card1)
	{
		if (Application::IsKeyPressed('E'))
		{
			render_card1 = false;
			carded = true;
		}
	}

	Box card2 = Box(Vector3(22, 0.6, -29), 2);
	collided_card2 = card2.isPointInBox(camera.position, card2);
	if (collided_card2)
	{
		if (Application::IsKeyPressed('E'))
		{
			render_card2 = false;
			carded = true;
		}
	}
}
