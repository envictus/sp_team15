#include "LOOT_CAMERA.h"


CameraLoot::CameraLoot()
{
}

CameraLoot::~CameraLoot()
{
}

void CameraLoot::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->up = defaultUp = right.Cross(view).Normalized();
}

void CameraLoot::Update(double dt, Object o[])
{
	//init movement and camera here
}

void CameraLoot::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}
