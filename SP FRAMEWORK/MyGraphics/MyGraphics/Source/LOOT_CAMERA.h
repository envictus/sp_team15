#ifndef CAMERA_LOOT
#define CAMERA_LOOT

#include "Camera.h"
#include "Object.h"
#include "Application.h"
#include "Mtx44.h"

class CameraLoot : public Camera
{
public:
	//Vector3 position;
	//Vector3 target;
	//Vector3 up;

	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;

	double xMouse;
	double yMouse;

	CameraLoot();
	~CameraLoot();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt, Object o[]);
	virtual void Reset();
};

#endif

