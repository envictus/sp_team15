#include "MAIN_CAMERA.h"
#include "Application.h"
#include "Mtx44.h"

#define COL 21

Camera3::Camera3()
{
}

Camera3::~Camera3()
{
}

void Camera3::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->up = defaultUp = right.Cross(view).Normalized();
}

void Camera3::Update(double dt, Object o[])
{
	//init movement and camera here
	static const float CAMERA_SPEED = 2.f;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	
	xMouse = 0;
	yMouse = 0;
	Application::GetMousePos(xMouse, yMouse);

	
	if (normalSpeed)
	{
		MOVEMENT_SPEED = 0.2f;
	}


	if (slowdown)
	{
		MOVEMENT_SPEED = 0.01f;
	}




	if (Application::IsKeyPressed('A'))
	{
		for (int i = 0; i < COL; i++)
		{
			if (((position.x - right.x) >= o[i].minX) && ((position.x - right.x) <= o[i].maxX))
			{
				if (((position.z - right.z) >= o[i].minZ) && ((position.z - right.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x - right.x) >= -78) && ((position.x - right.x) <= 24))
			{
				if (((position.z - right.z) >= -45) && ((position.z - right.z) <= 38))
				{
					position.z = position.z - right.z * MOVEMENT_SPEED;
					position.x = position.x - right.x * MOVEMENT_SPEED;
					target = position + view;
				}
			}
		}
		else
		{
			move = true;
		}
	}
	if (Application::IsKeyPressed('D'))
	{
		for (int i = 0; i < COL; i++)
		{
			if (((position.x + right.x) >= o[i].minX) && ((position.x + right.x) <= o[i].maxX))
			{
				if (((position.z + right.z) >= o[i].minZ) && ((position.z + right.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x + right.x) >= -78) && ((position.x + right.x) <= 24))
			{
				if (((position.z + right.z) >= -45) && ((position.z + right.z) <= 38))
				{
					position.z = position.z + right.z * MOVEMENT_SPEED;
					position.x = position.x + right.x * MOVEMENT_SPEED;
					target = position + view;
				}
			}
		}
		else
		{
			move = true;
		}
	}
	if (Application::IsKeyPressed('W'))
	{
		for (int i = 0; i < COL; i++)
		{
			if (((position.x + view.x) >= o[i].minX) && ((position.x + view.x) <= o[i].maxX))
			{
				if (((position.z + view.z) >= o[i].minZ) && ((position.z + view.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x + view.x) >= -78) && ((position.x + view.x) <= 24))
			{
				if (((position.z + view.z) >= -45) && ((position.z + view.z) <= 38))
				{
					position.z = position.z + view.z * MOVEMENT_SPEED;
					position.x = position.x + view.x * MOVEMENT_SPEED;
					target = position + view;
				}
			}
		}
		else
		{
			move = true;
		}
	}
	if (Application::IsKeyPressed('S'))
	{
		for (int i = 0; i < COL; i++)
		{
			if (((position.x - view.x) >= o[i].minX) && ((position.x - view.x) <= o[i].maxX))
			{
				if (((position.z - view.z) >= o[i].minZ) && ((position.z - view.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x - view.x) >= -78) && ((position.x - view.x) <= 24))
			{
				if (((position.z - view.z) >= -45) && ((position.z - view.z) <= 38))
				{
					position.z = position.z - view.z * MOVEMENT_SPEED;
					position.x = position.x - view.x * MOVEMENT_SPEED;
					target = position + view;
				}
			}
		}
		else
		{
			move = true;
		}
	}
	if (Application::IsKeyPressed('P'))
	{
		Reset();
	}
	if (yMouse < 360)
	{
		float pitch = (float)(CAMERA_SPEED * dt * (360 - yMouse));
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		if (view.y < 0.8)
		{
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
		}
	}
	if (yMouse > 360)
	{
		float pitch = (float)(CAMERA_SPEED * dt * (360 - yMouse));
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		if (view.y > -0.7)
		{
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
		}
	}
	if (xMouse < 640)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (640 - xMouse));
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);
		view = rotation * view;
		target = position + view;
	}
	if (xMouse > 640)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (640 - xMouse));
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);
		view = rotation * view;
		target = position + view;
	}
	Application::SetMousePos();



}

void Camera3::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}