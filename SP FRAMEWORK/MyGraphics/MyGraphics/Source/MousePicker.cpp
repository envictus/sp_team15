//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>

#include "MousePicker.h"

Vector3 mousepicker::calcMouseRay()
{
	Application::GetMousePos(mouseX, mouseY);

	Vector3 normalCoords = setNormalisedCoords(mouseX, mouseY);
	Vector4 clipCoords = Vector4(normalCoords.x, normalCoords.y, -1.0f, 1.0f);
	Vector4 eyeCoords = toEyeCoords(clipCoords);

	Vector3 worldRay = toWorldCoords(eyeCoords);

	return worldRay;
}

Vector3 mousepicker::setNormalisedCoords(double mouseX, double mouseY)
{
	float x = (2.0f * mouseX) / 1600 - 1;
	float y = (2.0f * mouseY) / 900 - 1.0f;
	float z = 1.0f;
	Vector3 normCoords = Vector3(x, y, z);

	return Vector3(normCoords.x, normCoords.y, normCoords.z);
}

Vector4 mousepicker::toEyeCoords(Vector4 clipCoords)
{
	Vector4 eyeCoords = projectionStack.Top().GetInverse() * clipCoords;
	eyeCoords = Vector4(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);

	return Vector4(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);
}

Vector3 mousepicker::toWorldCoords(Vector4 eyeCoords)
{
	Vector4 rayWorld = ViewMatrix.GetInverse() * eyeCoords;
	Vector3 mouseRay = Vector3(rayWorld.x, rayWorld.y, rayWorld.z);
	mouseRay.Normalize();

	return mouseRay;
}

void mousepicker::mousePicker(Camera3 camera, Mtx44 projection)
{
	this->camera = camera;
	this->ProjectionMatrix = projection;
	this->ViewMatrix = viewStack.Top();
}

Vector3 mousepicker::getCurrentRay()
{
	return currentRay;
}

void mousepicker::update()
{
	ViewMatrix = viewStack.Top();
	currentRay = calcMouseRay();
}

