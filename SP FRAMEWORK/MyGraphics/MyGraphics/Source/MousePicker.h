#include "Vector4.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Application.h"
#include "Mtx44.h"
#include "MAIN_CAMERA.h"
#include "MatrixStack.h"
#include "Utility.h"

class mousepicker
{
private:
	MS projectionStack, viewStack, modelStack;
	Vector3 currentRay;
	Mtx44 ViewMatrix;
	Camera3 camera;
	double mouseX;
	double mouseY;

	Vector3 calcMouseRay();
	Vector3 setNormalisedCoords(double mouseX, double mouseY);
	Vector4 toEyeCoords(Vector4 clipCoords);
	Vector3 toWorldCoords(Vector4 eyeCoords);

public:
	Mtx44 ProjectionMatrix;
	void mousePicker(Camera3 cam, Mtx44 projection);
	Vector3 getCurrentRay();
	void update();

};