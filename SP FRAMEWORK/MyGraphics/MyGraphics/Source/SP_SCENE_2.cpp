#include "SP_SCENE_2.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "Box.h"
#include "Object.h"
#include "SceneManager.h"

#include <iostream>

using namespace std;
Scene_2::Scene_2()
{
}

Scene_2::~Scene_2()
{
}

void Scene_2::Init()
{
	mouse_debounce = false;
	carded = false;
	render_card1 = true;
	render_card2 = true;
	door1_open = false;
	door2_open = false;

	trapped_s1 = false;
	trapped_s2 = false;
	trapped_s3 = false;
	trapped2 = false;
	trapped3 = false;
	trapped4 = false;

	trap_s1_end = false;
	trap_s2_end = false;
	trap_s3_end = false;
	trap2_end = false;
	trap3_end = false;
	trap4_end = false;

	inTrap_s1 = false;
	inTrap_s2 = false;
	inTrap_s3 = false;
	inTrap2 = false;
	inTrap3 = false;
	inTrap4 = false;

	esc_s_trap1 = 50;
	esc_s_trap2 = 50;
	esc_s_trap3 = 50;
	esc_trap2 = 50;
	esc_trap3 = 50;
	esc_trap4 = 50;

	mineblown = false;

	debuffTime = 30;
	debuff = 15;

	boom = 1;


	oof = 0;
	oof2 = 0;
	oof3 = 0;
	oof4 = 0;
	oof5 = 0;
	nemy = true;
	nemy2 = true;
	nemy3 = true;
	nemy4 = true;
	nemy5 = true;

	health = 100;

	weapon = false;
	consumable = false;

	render_bread = true;
	render_health = true;
	render_magazine = true;


	elapsedtime = 0;
	bread = 1;
	ammo = 1;

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	rotateGun = 0.0f;

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);

	camera.Init(Vector3(6, 1, -37), Vector3(1, 1, 1), Vector3(0, 1, 0));
	shoot.Set(1, 1, 1);

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 4000.f);
	projectionStack.LoadMatrix(projection);

	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");

	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");

	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");
	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");

	m_parameters[U_LIGHT2_TYPE] = glGetUniformLocation(m_programID, "lights[2].type");
	m_parameters[U_LIGHT2_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[2].spotDirection");
	m_parameters[U_LIGHT2_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[2].cosCutoff");
	m_parameters[U_LIGHT2_COSINNER] = glGetUniformLocation(m_programID, "lights[2].cosInner");
	m_parameters[U_LIGHT2_EXPONENT] = glGetUniformLocation(m_programID, "lights[2].exponent");
	m_parameters[U_LIGHT2_POSITION] = glGetUniformLocation(m_programID, "lights[2].position_cameraspace");
	m_parameters[U_LIGHT2_COLOR] = glGetUniformLocation(m_programID, "lights[2].color");
	m_parameters[U_LIGHT2_POWER] = glGetUniformLocation(m_programID, "lights[2].power");
	m_parameters[U_LIGHT2_KC] = glGetUniformLocation(m_programID, "lights[2].kC");
	m_parameters[U_LIGHT2_KL] = glGetUniformLocation(m_programID, "lights[2].kL");
	m_parameters[U_LIGHT2_KQ] = glGetUniformLocation(m_programID, "lights[2].kQ");

	m_parameters[U_LIGHT3_TYPE] = glGetUniformLocation(m_programID, "lights[3].type");
	m_parameters[U_LIGHT3_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[3].spotDirection");
	m_parameters[U_LIGHT3_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[3].cosCutoff");
	m_parameters[U_LIGHT3_COSINNER] = glGetUniformLocation(m_programID, "lights[3].cosInner");
	m_parameters[U_LIGHT3_EXPONENT] = glGetUniformLocation(m_programID, "lights[3].exponent");
	m_parameters[U_LIGHT3_POSITION] = glGetUniformLocation(m_programID, "lights[3].position_cameraspace");
	m_parameters[U_LIGHT3_COLOR] = glGetUniformLocation(m_programID, "lights[3].color");
	m_parameters[U_LIGHT3_POWER] = glGetUniformLocation(m_programID, "lights[3].power");
	m_parameters[U_LIGHT3_KC] = glGetUniformLocation(m_programID, "lights[3].kC");
	m_parameters[U_LIGHT3_KL] = glGetUniformLocation(m_programID, "lights[3].kL");
	m_parameters[U_LIGHT3_KQ] = glGetUniformLocation(m_programID, "lights[3].kQ");

	m_parameters[U_LIGHT4_TYPE] = glGetUniformLocation(m_programID, "lights[4].type");
	m_parameters[U_LIGHT4_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[4].spotDirection");
	m_parameters[U_LIGHT4_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[4].cosCutoff");
	m_parameters[U_LIGHT4_COSINNER] = glGetUniformLocation(m_programID, "lights[4].cosInner");
	m_parameters[U_LIGHT4_EXPONENT] = glGetUniformLocation(m_programID, "lights[4].exponent");
	m_parameters[U_LIGHT4_POSITION] = glGetUniformLocation(m_programID, "lights[4].position_cameraspace");
	m_parameters[U_LIGHT4_COLOR] = glGetUniformLocation(m_programID, "lights[4].color");
	m_parameters[U_LIGHT4_POWER] = glGetUniformLocation(m_programID, "lights[4].power");
	m_parameters[U_LIGHT4_KC] = glGetUniformLocation(m_programID, "lights[4].kC");
	m_parameters[U_LIGHT4_KL] = glGetUniformLocation(m_programID, "lights[4].kL");
	m_parameters[U_LIGHT4_KQ] = glGetUniformLocation(m_programID, "lights[4].kQ");

	m_parameters[U_LIGHT5_TYPE] = glGetUniformLocation(m_programID, "lights[5].type");
	m_parameters[U_LIGHT5_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[5].spotDirection");
	m_parameters[U_LIGHT5_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[5].cosCutoff");
	m_parameters[U_LIGHT5_COSINNER] = glGetUniformLocation(m_programID, "lights[5].cosInner");
	m_parameters[U_LIGHT5_EXPONENT] = glGetUniformLocation(m_programID, "lights[5].exponent");
	m_parameters[U_LIGHT5_POSITION] = glGetUniformLocation(m_programID, "lights[5].position_cameraspace");
	m_parameters[U_LIGHT5_COLOR] = glGetUniformLocation(m_programID, "lights[5].color");
	m_parameters[U_LIGHT5_POWER] = glGetUniformLocation(m_programID, "lights[5].power");
	m_parameters[U_LIGHT5_KC] = glGetUniformLocation(m_programID, "lights[5].kC");
	m_parameters[U_LIGHT5_KL] = glGetUniformLocation(m_programID, "lights[5].kL");
	m_parameters[U_LIGHT5_KQ] = glGetUniformLocation(m_programID, "lights[5].kQ");

	m_parameters[U_LIGHT6_TYPE] = glGetUniformLocation(m_programID, "lights[6].type");
	m_parameters[U_LIGHT6_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[6].spotDirection");
	m_parameters[U_LIGHT6_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[6].cosCutoff");
	m_parameters[U_LIGHT6_COSINNER] = glGetUniformLocation(m_programID, "lights[6].cosInner");
	m_parameters[U_LIGHT6_EXPONENT] = glGetUniformLocation(m_programID, "lights[6].exponent");
	m_parameters[U_LIGHT6_POSITION] = glGetUniformLocation(m_programID, "lights[6].position_cameraspace");
	m_parameters[U_LIGHT6_COLOR] = glGetUniformLocation(m_programID, "lights[6].color");
	m_parameters[U_LIGHT6_POWER] = glGetUniformLocation(m_programID, "lights[6].power");
	m_parameters[U_LIGHT6_KC] = glGetUniformLocation(m_programID, "lights[6].kC");
	m_parameters[U_LIGHT6_KL] = glGetUniformLocation(m_programID, "lights[6].kL");
	m_parameters[U_LIGHT6_KQ] = glGetUniformLocation(m_programID, "lights[6].kQ");

	m_parameters[U_LIGHT7_TYPE] = glGetUniformLocation(m_programID, "lights[7].type");
	m_parameters[U_LIGHT7_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[7].spotDirection");
	m_parameters[U_LIGHT7_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[7].cosCutoff");
	m_parameters[U_LIGHT7_COSINNER] = glGetUniformLocation(m_programID, "lights[7].cosInner");
	m_parameters[U_LIGHT7_EXPONENT] = glGetUniformLocation(m_programID, "lights[7].exponent");
	m_parameters[U_LIGHT7_POSITION] = glGetUniformLocation(m_programID, "lights[7].position_cameraspace");
	m_parameters[U_LIGHT7_COLOR] = glGetUniformLocation(m_programID, "lights[7].color");
	m_parameters[U_LIGHT7_POWER] = glGetUniformLocation(m_programID, "lights[7].power");
	m_parameters[U_LIGHT7_KC] = glGetUniformLocation(m_programID, "lights[7].kC");
	m_parameters[U_LIGHT7_KL] = glGetUniformLocation(m_programID, "lights[7].kL");
	m_parameters[U_LIGHT7_KQ] = glGetUniformLocation(m_programID, "lights[7].kQ");

	m_parameters[U_LIGHT8_TYPE] = glGetUniformLocation(m_programID, "lights[8].type");
	m_parameters[U_LIGHT8_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[8].spotDirection");
	m_parameters[U_LIGHT8_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[8].cosCutoff");
	m_parameters[U_LIGHT8_COSINNER] = glGetUniformLocation(m_programID, "lights[8].cosInner");
	m_parameters[U_LIGHT8_EXPONENT] = glGetUniformLocation(m_programID, "lights[8].exponent");
	m_parameters[U_LIGHT8_POSITION] = glGetUniformLocation(m_programID, "lights[8].position_cameraspace");
	m_parameters[U_LIGHT8_COLOR] = glGetUniformLocation(m_programID, "lights[8].color");
	m_parameters[U_LIGHT8_POWER] = glGetUniformLocation(m_programID, "lights[8].power");
	m_parameters[U_LIGHT8_KC] = glGetUniformLocation(m_programID, "lights[8].kC");
	m_parameters[U_LIGHT8_KL] = glGetUniformLocation(m_programID, "lights[8].kL");
	m_parameters[U_LIGHT8_KQ] = glGetUniformLocation(m_programID, "lights[8].kQ");

	m_parameters[U_LIGHT9_TYPE] = glGetUniformLocation(m_programID, "lights[9].type");
	m_parameters[U_LIGHT9_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[9].spotDirection");
	m_parameters[U_LIGHT9_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[9].cosCutoff");
	m_parameters[U_LIGHT9_COSINNER] = glGetUniformLocation(m_programID, "lights[9].cosInner");
	m_parameters[U_LIGHT9_EXPONENT] = glGetUniformLocation(m_programID, "lights[9].exponent");
	m_parameters[U_LIGHT9_POSITION] = glGetUniformLocation(m_programID, "lights[9].position_cameraspace");
	m_parameters[U_LIGHT9_COLOR] = glGetUniformLocation(m_programID, "lights[9].color");
	m_parameters[U_LIGHT9_POWER] = glGetUniformLocation(m_programID, "lights[9].power");
	m_parameters[U_LIGHT9_KC] = glGetUniformLocation(m_programID, "lights[9].kC");
	m_parameters[U_LIGHT9_KL] = glGetUniformLocation(m_programID, "lights[9].kL");
	m_parameters[U_LIGHT9_KQ] = glGetUniformLocation(m_programID, "lights[9].kQ");

	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights"); //in case you missed out practical 7																		// Get a handle for our "colorTexture" uniform
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");

	glUseProgram(m_programID);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	light[0].type = Light::LIGHT_DIRECTIONAL;
	light[0].position.Set(0, 5, 0);
	light[0].color.Set(1, 1, 1);
	light[0].power = 1;
	light[0].kC = 1.f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	light[0].cosInner = cos(Math::DegreeToRadian(30));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(0.f, 1.f, 0.f);

	light[1].type = Light::LIGHT_SPOT;
	light[1].position.Set(1, 15, -35);
	light[1].color.Set(1, 1, 1);
	light[1].power = 0.5;
	light[1].kC = 1.f;
	light[1].kL = 0.01f;
	light[1].kQ = 0.001f;
	light[1].cosCutoff = cos(Math::DegreeToRadian(45));
	light[1].cosInner = cos(Math::DegreeToRadian(30));
	light[1].exponent = 3.f;
	light[1].spotDirection.Set(0.f, 1.f, 0.f);

	light[2].type = Light::LIGHT_SPOT;
	light[2].position.Set(-35, 15, -24);
	light[2].color.Set(1, 1, 1);
	light[2].power = 0.5;
	light[2].kC = 1.f;
	light[2].kL = 0.01f;
	light[2].kQ = 0.001f;
	light[2].cosCutoff = cos(Math::DegreeToRadian(45));
	light[2].cosInner = cos(Math::DegreeToRadian(30));
	light[2].exponent = 3.f;
	light[2].spotDirection.Set(0.f, 1.f, 0.f);

	light[3].type = Light::LIGHT_SPOT;
	light[3].position.Set(-67, 15, -31);
	light[3].color.Set(1, 1, 1);
	light[3].power = 0.5;
	light[3].kC = 1.f;
	light[3].kL = 0.01f;
	light[3].kQ = 0.001f;
	light[3].cosCutoff = cos(Math::DegreeToRadian(45));
	light[3].cosInner = cos(Math::DegreeToRadian(30));
	light[3].exponent = 3.f;
	light[3].spotDirection.Set(0.f, 1.f, 0.f);

	light[4].type = Light::LIGHT_SPOT;
	light[4].position.Set(-53, 20, -1);
	light[4].color.Set(1, 1, 1);
	light[4].power = 1.5;
	light[4].kC = 1.f;
	light[4].kL = 0.01f;
	light[4].kQ = 0.001f;
	light[4].cosCutoff = cos(Math::DegreeToRadian(45));
	light[4].cosInner = cos(Math::DegreeToRadian(30));
	light[4].exponent = 3.f;
	light[4].spotDirection.Set(0.f, 1.f, 0.f);

	light[5].type = Light::LIGHT_SPOT;
	light[5].position.Set(-53, 20, 26);
	light[5].color.Set(1, 1, 1);
	light[5].power = 1.5;
	light[5].kC = 1.f;
	light[5].kL = 0.01f;
	light[5].kQ = 0.001f;
	light[5].cosCutoff = cos(Math::DegreeToRadian(45));
	light[5].cosInner = cos(Math::DegreeToRadian(30));
	light[5].exponent = 3.f;
	light[5].spotDirection.Set(0.f, 1.f, 0.f);

	light[6].type = Light::LIGHT_SPOT;
	light[6].position.Set(-5, 17, -3);
	light[6].color.Set(1, 1, 1);
	light[6].power = 0.5;
	light[6].kC = 1.f;
	light[6].kL = 0.01f;
	light[6].kQ = 0.001f;
	light[6].cosCutoff = cos(Math::DegreeToRadian(45));
	light[6].cosInner = cos(Math::DegreeToRadian(30));
	light[6].exponent = 3.f;
	light[6].spotDirection.Set(0.f, 1.f, 0.f);

	light[7].type = Light::LIGHT_SPOT;
	light[7].position.Set(-15, 17, 22);
	light[7].color.Set(1, 1, 1);
	light[7].power = 0.5;
	light[7].kC = 1.f;
	light[7].kL = 0.01f;
	light[7].kQ = 0.001f;
	light[7].cosCutoff = cos(Math::DegreeToRadian(45));
	light[7].cosInner = cos(Math::DegreeToRadian(30));
	light[7].exponent = 3.f;
	light[7].spotDirection.Set(0.f, 1.f, 0.f);

	light[8].type = Light::LIGHT_SPOT;
	light[8].position.Set(16, 17, 31);
	light[8].color.Set(1, 1, 1);
	light[8].power = 0.5;
	light[8].kC = 1.f;
	light[8].kL = 0.01f;
	light[8].kQ = 0.001f;
	light[8].cosCutoff = cos(Math::DegreeToRadian(45));
	light[8].cosInner = cos(Math::DegreeToRadian(30));
	light[8].exponent = 3.f;
	light[8].spotDirection.Set(0.f, 1.f, 0.f);

	light[9].type = Light::LIGHT_SPOT;
	light[9].position.Set(18, 17, -5);
	light[9].color.Set(1, 1, 1);
	light[9].power = 0.5;
	light[9].kC = 1.f;
	light[9].kL = 0.01f;
	light[9].kQ = 0.001f;
	light[9].cosCutoff = cos(Math::DegreeToRadian(45));
	light[9].cosInner = cos(Math::DegreeToRadian(30));
	light[9].exponent = 3.f;
	light[9].spotDirection.Set(0.f, 1.f, 0.f);

	// Make sure you pass uniform parameters after glUseProgram()
	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);

	glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);
	glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT1_COSINNER], light[1].cosInner);
	glUniform1f(m_parameters[U_LIGHT1_EXPONENT], light[1].exponent);

	glUniform1i(m_parameters[U_LIGHT2_TYPE], light[2].type);
	glUniform3fv(m_parameters[U_LIGHT2_COLOR], 1, &light[2].color.r);
	glUniform1f(m_parameters[U_LIGHT2_POWER], light[2].power);
	glUniform1f(m_parameters[U_LIGHT2_KC], light[2].kC);
	glUniform1f(m_parameters[U_LIGHT2_KL], light[2].kL);
	glUniform1f(m_parameters[U_LIGHT2_KQ], light[2].kQ);
	glUniform1f(m_parameters[U_LIGHT2_COSCUTOFF], light[2].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT2_COSINNER], light[2].cosInner);
	glUniform1f(m_parameters[U_LIGHT2_EXPONENT], light[2].exponent);

	glUniform1i(m_parameters[U_LIGHT3_TYPE], light[3].type);
	glUniform3fv(m_parameters[U_LIGHT3_COLOR], 1, &light[3].color.r);
	glUniform1f(m_parameters[U_LIGHT3_POWER], light[3].power);
	glUniform1f(m_parameters[U_LIGHT3_KC], light[3].kC);
	glUniform1f(m_parameters[U_LIGHT3_KL], light[3].kL);
	glUniform1f(m_parameters[U_LIGHT3_KQ], light[3].kQ);
	glUniform1f(m_parameters[U_LIGHT3_COSCUTOFF], light[3].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT3_COSINNER], light[3].cosInner);
	glUniform1f(m_parameters[U_LIGHT3_EXPONENT], light[3].exponent);

	glUniform1i(m_parameters[U_LIGHT4_TYPE], light[4].type);
	glUniform3fv(m_parameters[U_LIGHT4_COLOR], 1, &light[4].color.r);
	glUniform1f(m_parameters[U_LIGHT4_POWER], light[4].power);
	glUniform1f(m_parameters[U_LIGHT4_KC], light[4].kC);
	glUniform1f(m_parameters[U_LIGHT4_KL], light[4].kL);
	glUniform1f(m_parameters[U_LIGHT4_KQ], light[4].kQ);
	glUniform1f(m_parameters[U_LIGHT4_COSCUTOFF], light[4].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT4_COSINNER], light[4].cosInner);
	glUniform1f(m_parameters[U_LIGHT4_EXPONENT], light[4].exponent);

	glUniform1i(m_parameters[U_LIGHT5_TYPE], light[5].type);
	glUniform3fv(m_parameters[U_LIGHT5_COLOR], 1, &light[5].color.r);
	glUniform1f(m_parameters[U_LIGHT5_POWER], light[5].power);
	glUniform1f(m_parameters[U_LIGHT5_KC], light[5].kC);
	glUniform1f(m_parameters[U_LIGHT5_KL], light[5].kL);
	glUniform1f(m_parameters[U_LIGHT5_KQ], light[5].kQ);
	glUniform1f(m_parameters[U_LIGHT5_COSCUTOFF], light[5].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT5_COSINNER], light[5].cosInner);
	glUniform1f(m_parameters[U_LIGHT5_EXPONENT], light[5].exponent);

	glUniform1i(m_parameters[U_LIGHT6_TYPE], light[6].type);
	glUniform3fv(m_parameters[U_LIGHT6_COLOR], 1, &light[6].color.r);
	glUniform1f(m_parameters[U_LIGHT6_POWER], light[6].power);
	glUniform1f(m_parameters[U_LIGHT6_KC], light[6].kC);
	glUniform1f(m_parameters[U_LIGHT6_KL], light[6].kL);
	glUniform1f(m_parameters[U_LIGHT6_KQ], light[6].kQ);
	glUniform1f(m_parameters[U_LIGHT6_COSCUTOFF], light[6].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT6_COSINNER], light[6].cosInner);
	glUniform1f(m_parameters[U_LIGHT6_EXPONENT], light[6].exponent);

	glUniform1i(m_parameters[U_LIGHT7_TYPE], light[7].type);
	glUniform3fv(m_parameters[U_LIGHT7_COLOR], 1, &light[7].color.r);
	glUniform1f(m_parameters[U_LIGHT7_POWER], light[7].power);
	glUniform1f(m_parameters[U_LIGHT7_KC], light[7].kC);
	glUniform1f(m_parameters[U_LIGHT7_KL], light[7].kL);
	glUniform1f(m_parameters[U_LIGHT7_KQ], light[7].kQ);
	glUniform1f(m_parameters[U_LIGHT7_COSCUTOFF], light[7].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT7_COSINNER], light[7].cosInner);
	glUniform1f(m_parameters[U_LIGHT7_EXPONENT], light[7].exponent);

	glUniform1i(m_parameters[U_LIGHT8_TYPE], light[8].type);
	glUniform3fv(m_parameters[U_LIGHT8_COLOR], 1, &light[8].color.r);
	glUniform1f(m_parameters[U_LIGHT8_POWER], light[8].power);
	glUniform1f(m_parameters[U_LIGHT8_KC], light[8].kC);
	glUniform1f(m_parameters[U_LIGHT8_KL], light[8].kL);
	glUniform1f(m_parameters[U_LIGHT8_KQ], light[8].kQ);
	glUniform1f(m_parameters[U_LIGHT8_COSCUTOFF], light[8].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT8_COSINNER], light[8].cosInner);
	glUniform1f(m_parameters[U_LIGHT8_EXPONENT], light[8].exponent);

	glUniform1i(m_parameters[U_LIGHT9_TYPE], light[9].type);
	glUniform3fv(m_parameters[U_LIGHT9_COLOR], 1, &light[9].color.r);
	glUniform1f(m_parameters[U_LIGHT9_POWER], light[9].power);
	glUniform1f(m_parameters[U_LIGHT9_KC], light[9].kC);
	glUniform1f(m_parameters[U_LIGHT9_KL], light[9].kL);
	glUniform1f(m_parameters[U_LIGHT9_KQ], light[9].kQ);
	glUniform1f(m_parameters[U_LIGHT9_COSCUTOFF], light[9].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT9_COSINNER], light[9].cosInner);
	glUniform1f(m_parameters[U_LIGHT9_EXPONENT], light[9].exponent);

	glUniform1i(m_parameters[U_NUMLIGHTS], 10);
	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	//skybox
	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad("front", Color(1, 1, 1), 1003.f, 1003.f);
	meshList[GEO_FRONT]->textureID = LoadTGA("Image//front.tga");
	meshList[GEO_BACK] = MeshBuilder::GenerateQuad("back", Color(1, 1, 1), 1003.f, 1003.f);
	meshList[GEO_BACK]->textureID = LoadTGA("Image//back.tga");
	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad("left", Color(1, 1, 1), 1003.f, 1003.f);
	meshList[GEO_LEFT]->textureID = LoadTGA("Image//left.tga");
	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad("right", Color(1, 1, 1), 1003.f, 1003.f);
	meshList[GEO_RIGHT]->textureID = LoadTGA("Image//right.tga");
	meshList[GEO_TOP] = MeshBuilder::GenerateQuad("top", Color(1, 1, 1), 1003.f, 1003.f);
	meshList[GEO_TOP]->textureID = LoadTGA("Image//top.tga");
	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(1, 1, 1), 1003.f, 1003.f);
	meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//bottom.tga");

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000, 1000, 1000);
	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("lightBall", Color(1, 1, 1), 9, 36, 1);
	meshList[GEO_LIGHTBALL_CEILING] = MeshBuilder::GenerateSphere("lightBall_ceiling", Color(1, 1, 1), 9, 36, 1);
	meshList[GEO_LIGHTBALL_CEILING] = MeshBuilder::GenerateSphere("lightBall_ceiling", Color(1, 1, 1), 9, 36, 1);

	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_MAP326] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-6).obj");
	meshList[GEO_MAP326]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP328] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-8).obj");
	meshList[GEO_MAP328]->textureID = LoadTGA("Image//map3(2-14).tga");
	meshList[GEO_MAP3212] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-12).obj");
	meshList[GEO_MAP3212]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP3214] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-14).obj");
	meshList[GEO_MAP3214]->textureID = LoadTGA("Image//map3(2-14).tga");
	meshList[GEO_MAP3218] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-18).obj");
	meshList[GEO_MAP3218]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP3224] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-24).obj");
	meshList[GEO_MAP3224]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP3230] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-30).obj");
	meshList[GEO_MAP3230]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP3236] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-36).obj");
	meshList[GEO_MAP3236]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP3290] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-90).obj");
	meshList[GEO_MAP3290]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP32102] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(2-102).obj");
	meshList[GEO_MAP32102]->textureID = LoadTGA("Image//map3(2-18).tga");
	meshList[GEO_MAP366] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(6-6).obj");
	meshList[GEO_MAP366]->textureID = LoadTGA("Image//map3(6-6).tga");
	meshList[GEO_MAP31218] = MeshBuilder::GenerateOBJ("map", "OBJ//map3(12-18).obj");
	meshList[GEO_MAP31218]->textureID = LoadTGA("Image//map3(6-6).tga");

	meshList[GEO_CARD] = MeshBuilder::GenerateOBJ("card", "OBJ//card.obj");
	meshList[GEO_CARD]->textureID = LoadTGA("Image//card.tga");

	meshList[GEO_DOOR] = MeshBuilder::GenerateOBJ("door", "OBJ//door.obj");
	meshList[GEO_DOOR]->textureID = LoadTGA("Image//door.tga");

	meshList[GEO_ARM] = MeshBuilder::GenerateOBJ("arm", "OBJ//arm.obj");
	meshList[GEO_ARM]->textureID = LoadTGA("Image//arm.tga");

	meshList[GEO_GUN] = MeshBuilder::GenerateOBJ("gun", "OBJ//pistol.obj");
	meshList[GEO_GUN]->textureID = LoadTGA("Image//pistol.tga");

	meshList[GEO_WIRE] = MeshBuilder::GenerateOBJ("wire", "OBJ//wire.obj");
	meshList[GEO_WIRE]->textureID = LoadTGA("Image//wire.tga");

	meshList[GEO_MINE] = MeshBuilder::GenerateOBJ("mine", "OBJ//mine.obj");
	meshList[GEO_MINE]->textureID = LoadTGA("Image//mine.tga");

	meshList[GEO_BEARTRAP] = MeshBuilder::GenerateOBJ("beartrap", "OBJ//beartrap.obj");
	meshList[GEO_BEARTRAP]->textureID = LoadTGA("Image//beartrap.tga");

	meshList[GEO_EXPLODE] = MeshBuilder::GenerateQuad("explode", Color(1, 1, 1), 2.f, 2.f);
	meshList[GEO_EXPLODE]->textureID = LoadTGA("Image//kablamo.tga");

	meshList[GEO_SPIKE] = MeshBuilder::GenerateOBJ("Spike", "OBJ//Spike.obj");

	meshList[GEO_PISTOLPIC] = MeshBuilder::GenerateQuad("pistolpic", Color(1, 1, 1), 2.f, 2.f);
	meshList[GEO_PISTOLPIC]->textureID = LoadTGA("Image//PistolPic.tga");
	meshList[GEO_AK74PIC] = MeshBuilder::GenerateQuad("ak74pic", Color(1, 1, 1), 2.f, 2.f);
	meshList[GEO_AK74PIC]->textureID = LoadTGA("Image//AK74Pic.tga");
	meshList[GEO_SMGPIC] = MeshBuilder::GenerateQuad("smgpic", Color(1, 1, 1), 2.f, 2.f);
	meshList[GEO_SMGPIC]->textureID = LoadTGA("Image//MachineGunPic.tga");

	meshList[GEO_HP4] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_HP4]->textureID = LoadTGA("Image//hp_4.tga");
	meshList[GEO_HP5] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_HP5]->textureID = LoadTGA("Image//hp_5.tga");
	meshList[GEO_HP6] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_HP6]->textureID = LoadTGA("Image//hp_6.tga");
	meshList[GEO_AMMO] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_AMMO]->textureID = LoadTGA("Image//ammo.tga");
	meshList[GEO_OMAEWAMOSHINDEIRU] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_OMAEWAMOSHINDEIRU]->textureID = LoadTGA("Image//omaewamoshindeiru.tga");
	meshList[GEO_BREADUI] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_BREADUI]->textureID = LoadTGA("Image//breadui.tga");
	meshList[GEO_CARDUI] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 10.f, 10.f);
	meshList[GEO_CARDUI]->textureID = LoadTGA("Image//cardui.tga");

	meshList[GEO_MAGAZINE] = MeshBuilder::GenerateOBJ("map", "OBJ//magazine.obj");
	meshList[GEO_MAGAZINE]->textureID = LoadTGA("Image//magazine.tga");
	meshList[GEO_HEALTH] = MeshBuilder::GenerateOBJ("health", "OBJ//healthkit.obj");
	meshList[GEO_HEALTH]->textureID = LoadTGA("Image//healthkit.tga");
	meshList[GEO_BREAD] = MeshBuilder::GenerateOBJ("bread", "OBJ//Bread.obj");
	meshList[GEO_BREAD]->textureID = LoadTGA("Image//Bread.tga");
	meshList[GEO_ENEMY] = MeshBuilder::GenerateOBJ("enemytest", "OBJ//enemytest.obj");
	meshList[GEO_ENEMY]->textureID = LoadTGA("Image//enemytest.tga");
	//init booleans here

	//init objects here

	//init object min max vertices here
	object[0] = { 6, 10, 20, 30 };
	object[1] = { -4, 10, 20, 24 };
	object[2] = { 6, 9, 7.5, 27.5 };
	object[3] = { -28, 9.5, 25.5, 30 };
	object[4] = { -28, -24, -16, 30 };
	object[5] = { -27.5, -12.5, -17.5, -14.5 };
	object[6] = { -11.5, -4, -17.5, -14.5 };
	object[7] = { -8, 10, -26, -22 };
	object[8] = { -2, 1.5, -16, 21 };
	object[9] = { -22, -18.5, -25, -16 };
	object[10] = { -31.5, -28, -35.5, -26.5 };
	object[11] = { -31.5, -8.5, -35.5, -32 };
	object[12] = { -34, -14.5, -40, -36.5 };
	object[13] = { -11.5, -8.5, -45, -32 };
	object[14] = { -31.5, 5.5, -30, -27 };
	object[15] = { 6, 10, -42, -22.5 };
	object[16] = { -8, -4, -25, -16 };
	object[17] = { -34, -18, -25.5, -22 };
	object[18] = { -35.5, -33, -40, -22 };
	object[19] = { 6, 10, -42, -22 };
	object[19] = { 2.5, 10, -44, -40 };
}

void Scene_2::BoundsCheck()
{
	system("CLS");
	cout << camera.position << endl;
	cout << esc_s_trap1 << endl;
	cout << esc_trap2 << endl;
	cout << esc_trap3 << endl;
	cout << carded << endl;

	cout << debuffTime << endl;
	cout << debuff << endl;
	cout << boom << endl;

	CardCheck();
	DoorCheck();
	SlowdownTrapCheck1();
	SlowdownTrapCheck2();
	SlowdownTrapCheck3();
	InjureTrapCheck();
	PoisonTrapCheck();
}

void Scene_2::Update(double dt)
{
	//init update specific data here
	static const float LSPEED = 10.0f;

	if (Application::IsKeyPressed('1'))
	{
		glEnable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('2'))
	{
		glDisable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('3'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (Application::IsKeyPressed('4'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	light[0].position.x = camera.position.x;
	light[0].position.z = camera.position.z;

	//init update loops here
	if (Application::IsKeyPressed(VK_RBUTTON)) //if ads
	{
		//iron_sights.Lerp(Vector3(0.01, -0.4, -0.05), 0.5);
		shoot.Lerp(Vector3(0.01, -0.4, -0.05), 0.5);
	}
	else //regular
	{
		//iron_sights.Lerp(Vector3(0.5, -0.5, -0.8), 0.5);
		shoot.Lerp(Vector3(0.5, -0.5, -0.8), 0.5);
	}

	//init update loops here
	if (Application::IsKeyPressed(VK_LBUTTON))
	{
		if (!mouse_debounce) //can shoot
		{
			rotateGun = 30.0f;
			if (Application::IsKeyPressed(VK_RBUTTON)) //special ads shoot
			{
				shoot.Lerp(Vector3(0.01, -0.4, -0.35), 0.5);
			}
			else //regular shoot
			{
				shoot.Lerp(Vector3(0.5, -0.5, -0.5), 0.5);
			}
			mouse_debounce = true;

			bullets.push_back(new Projectile);
			bullets[bullets.size() - 1]->Shooting.position = camera.position;
			bullets[bullets.size() - 1]->Shooting.target = camera.target;
		}
	}
	else
	{
		mouse_debounce = false;
	}

	rotateGun = rotateGun + (0.0f - rotateGun) * 0.5f;
	camera.Update(dt, object);
	BoundsCheck();
	cout << oof << endl;
	cout << oof2 << endl;

	if (nemy == true)
	{
		EnemyMoving(dt);
		EnemyHealth();
	}
	else if (nemy == false)
	{
		oof = 0;
	}

	if (nemy2 == true)
	{
		EnemyMoving2(dt);
		EnemyHealth2();
	}
	else if (nemy2 == false)
	{
		oof2 = 0;
	}

	if (nemy3 == true)
	{
		EnemyMoving3(dt);
		EnemyHealth3();
	}
	else if (nemy3 == false)
	{
		oof3 = 0;
	}

	if (nemy4 == true)
	{
		EnemyMoving4(dt);
		EnemyHealth4();
	}
	else if (nemy4 == false)
	{
		oof4 = 0;
	}

	if (nemy5 == true)
	{
		EnemyMoving5(dt);
		EnemyHealth5();
	}
	else if (nemy5 == false)
	{
		oof5 = 0;
	}


	Vector3 enemyPos = { enemy.x, 1.0f, enemy.z };
	Vector3 enemy2Pos = { enemy2.x, 1.0f, enemy2.z };
	Vector3 enemy3Pos = { enemy2.x, 1.0f, enemy3.z };
	Vector3 enemy4Pos = { enemy4.x, 1.0f, enemy4.z };
	Vector3 enemy5Pos = { enemy5.x, 1.0f, enemy5.z };


	FPS = 1.0 / dt;
	framerate = std::to_string(FPS);
	const unsigned char FPS = 60; // FPS of this game
	const unsigned int frameTime = 1000 / FPS; // time for each frame
	cout << "health: " << health << endl;
	cout << "framerate: " << framerate << endl;

	//check box x, y, z position
	//if collided delete and minus health
	if (bullets.size() > 0) {
		Box thing = Box(Vector3(bullets[bullets.size() - 1]->Shooting.position.x, bullets[bullets.size() - 1]->Shooting.position.y, bullets[bullets.size() - 1]->Shooting.position.z), 1);
		collided = thing.isPointInBox(enemyPos, thing);
		if (collided)
		{
			oof++;
			if (oof == 50)
			{
				nemy = false;
			}
		}
	}


	for (int i = bullets.size() - 1; i < -1; i--) {
		bullets[i]->Shooting.move();
		if (collided)
		{
			delete bullets[i];
		}
	}

	if (bullets.size() > 0) {
		Box thing2 = Box(Vector3(bullets[bullets.size() - 1]->Shooting.position.x, bullets[bullets.size() - 1]->Shooting.position.y, bullets[bullets.size() - 1]->Shooting.position.z), 1);
		collided2 = thing2.isPointInBox(enemy2Pos, thing2);
		if (collided2)
		{
			oof2++;
			if (oof2 == 50)
			{
				//Sound3();
				nemy2 = false;
			}
		}
	}


	for (int i = bullets.size() - 1; i < -1; i--) {
		bullets[i]->Shooting.move();
		if (collided2)
		{
			delete bullets[i];
		}
	}


	if (bullets.size() > 0) {
		Box thing3 = Box(Vector3(bullets[bullets.size() - 1]->Shooting.position.x, bullets[bullets.size() - 1]->Shooting.position.y, bullets[bullets.size() - 1]->Shooting.position.z), 1);
		collided3 = thing3.isPointInBox(enemy3Pos, thing3);
		if (collided3)
		{
			oof3++;
			if (oof3 == 50)
			{
				nemy3 = false;
			}
		}
	}


	for (int i = bullets.size() - 1; i < -1; i--) {
		bullets[i]->Shooting.move();
		if (collided3)
		{
			delete bullets[i];
		}
	}

	if (bullets.size() > 0) {
		Box thing4 = Box(Vector3(bullets[bullets.size() - 1]->Shooting.position.x, bullets[bullets.size() - 1]->Shooting.position.y, bullets[bullets.size() - 1]->Shooting.position.z), 1);
		collided4 = thing4.isPointInBox(enemy4Pos, thing4);
		if (collided4)
		{
			oof4++;
			if (oof4 == 50)
			{
				nemy4 = false;
			}
		}
	}


	for (int i = bullets.size() - 1; i < -1; i--) {
		bullets[i]->Shooting.move();
		if (collided4)
		{
			delete bullets[i];
		}
	}

	if (bullets.size() > 0) {
		Box thing5 = Box(Vector3(bullets[bullets.size() - 1]->Shooting.position.x, bullets[bullets.size() - 1]->Shooting.position.y, bullets[bullets.size() - 1]->Shooting.position.z), 1);
		collided5 = thing5.isPointInBox(enemy5Pos, thing5);
		if (collided5)
		{
			oof5++;
			if (oof5 == 50)
			{
				nemy5 = false;
			}
		}
	}


	for (int i = bullets.size() - 1; i < -1; i--) {
		bullets[i]->Shooting.move();
		if (collided5)
		{
			delete bullets[i];
		}
	}

	if (health < 0)
	{
		SceneManager::instance()->SetNextScene(4);
	}
}

void Scene_2::mousePicking()
{
	Box collobox = Box(Vector3(-7, 1, 20), 3);
	CRay raythingo(camera.position, (camera.target - camera.position).Normalized());
	double distbetweenboxandrayposition = (collobox.position - raythingo.GetPosition()).Length();
	//rayposthing = raythingo.PointOnRay(distbetweenboxandrayposition);
	/*if (collobox.isPointInBox(raythingo.PointOnRay(distbetweenboxandrayposition),collobox))
	std::cout << "hienloe" << std::endl;*/

	if (distbetweenboxandrayposition < (double)10)
	{
		if (collobox.isPointInBox(raythingo.PointOnRay(distbetweenboxandrayposition), collobox))
			std::cout << "hienloe" << std::endl;
	}

}

void Scene_2::Render()
{
	if (light[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[0].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}



	if (light[1].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[1].position.x, light[1].position.y, light[1].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[1].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[1].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	}



	if (light[2].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[2].position.x, light[2].position.y, light[2].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT2_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[2].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[2].position;
		glUniform3fv(m_parameters[U_LIGHT2_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[2].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT2_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[2].position;
		glUniform3fv(m_parameters[U_LIGHT2_POSITION], 1, &lightPosition_cameraspace.x);
	}



	if (light[3].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[3].position.x, light[3].position.y, light[3].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT3_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[3].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[3].position;
		glUniform3fv(m_parameters[U_LIGHT3_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[3].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT3_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[3].position;
		glUniform3fv(m_parameters[U_LIGHT3_POSITION], 1, &lightPosition_cameraspace.x);
	}



	if (light[4].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[4].position.x, light[4].position.y, light[4].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT4_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[4].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[4].position;
		glUniform3fv(m_parameters[U_LIGHT4_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[4].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT4_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[3].position;
		glUniform3fv(m_parameters[U_LIGHT3_POSITION], 1, &lightPosition_cameraspace.x);
	}




	if (light[5].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[5].position.x, light[5].position.y, light[5].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT5_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[5].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[5].position;
		glUniform3fv(m_parameters[U_LIGHT5_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[5].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT5_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[5].position;
		glUniform3fv(m_parameters[U_LIGHT5_POSITION], 1, &lightPosition_cameraspace.x);
	}




	if (light[6].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[6].position.x, light[6].position.y, light[6].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT6_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[6].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[6].position;
		glUniform3fv(m_parameters[U_LIGHT6_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[6].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT6_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[6].position;
		glUniform3fv(m_parameters[U_LIGHT6_POSITION], 1, &lightPosition_cameraspace.x);
	}




	if (light[7].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[7].position.x, light[7].position.y, light[7].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT7_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[7].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[7].position;
		glUniform3fv(m_parameters[U_LIGHT7_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[7].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT7_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[7].position;
		glUniform3fv(m_parameters[U_LIGHT7_POSITION], 1, &lightPosition_cameraspace.x);
	}



	if (light[8].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[8].position.x, light[8].position.y, light[8].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT8_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[8].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[8].position;
		glUniform3fv(m_parameters[U_LIGHT8_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[8].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT8_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[8].position;
		glUniform3fv(m_parameters[U_LIGHT8_POSITION], 1, &lightPosition_cameraspace.x);
	}


	if (light[9].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[9].position.x, light[9].position.y, light[9].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT9_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[9].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[9].position;
		glUniform3fv(m_parameters[U_LIGHT9_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[9].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT9_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[9].position;
		glUniform3fv(m_parameters[U_LIGHT9_POSITION], 1, &lightPosition_cameraspace.x);
	}


	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
	glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);

	RenderModel();
	//call obj specific render functions here
	RenderCard();
	RenderDoor();
	RenderSlowdownTrap1();
	RenderSlowdownTrap2();
	RenderSlowdownTrap3();
	RenderInjureTrap();
	RenderPoisonTrap();
	RenderStopTrap();

	modelStack.PushMatrix();
	modelStack.Translate(light[0].position.x, light[0].position.y, light[0].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[1].position.x, light[1].position.y, light[1].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();


	modelStack.PushMatrix();
	modelStack.Translate(light[2].position.x, light[2].position.y, light[2].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[3].position.x, light[3].position.y, light[3].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[4].position.x, light[4].position.y, light[4].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[5].position.x, light[5].position.y, light[5].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[6].position.x, light[6].position.y, light[6].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[7].position.x, light[7].position.y, light[7].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[8].position.x, light[8].position.y, light[8].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(light[9].position.x, light[9].position.y, light[9].position.z);
	RenderMesh(meshList[GEO_LIGHTBALL_CEILING], false);
	modelStack.PopMatrix();

	//render generic objects here
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(46, 0, -20);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(42, 0, 0);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-30, 0, -38);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-16, 0, -48);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-6, 0, -26);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-20, 0, -26);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-26, 0, 18);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(-22, 0, 0);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(-22, 0, -6);
	RenderMesh(meshList[GEO_MAP326], true);
	modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Rotate(90, 0, 1, 0);
	//RenderMesh(meshList[GEO_MAP328], true);
	//modelStack.PopMatrix();
	//modelStack.PushMatrix();
	//modelStack.Rotate(90, 0, 1, 0);
	//RenderMesh(meshList[GEO_MAP328], true);
	//modelStack.PopMatrix();
	//modelStack.PushMatrix();
	//modelStack.Rotate(90, 0, 1, 0);
	//RenderMesh(meshList[GEO_MAP328], true);
	//modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(16, 0, -2);
	RenderMesh(meshList[GEO_MAP3212], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(16, 0, -18);
	RenderMesh(meshList[GEO_MAP3212], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-10, 0, -38);
	RenderMesh(meshList[GEO_MAP3212], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(24, 0, 2);
	RenderMesh(meshList[GEO_MAP3214], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(24, 0, -24);
	RenderMesh(meshList[GEO_MAP3214], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(38, 0, -24);
	RenderMesh(meshList[GEO_MAP3218], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(34, 0, -20);
	RenderMesh(meshList[GEO_MAP3218], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(4, 0, -38);
	RenderMesh(meshList[GEO_MAP3218], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(8, 0, -32);
	RenderMesh(meshList[GEO_MAP3218], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(8, 0, 18);
	RenderMesh(meshList[GEO_MAP3218], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-34, 0, -32);
	RenderMesh(meshList[GEO_MAP3218], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(-28, 0, -12);
	RenderMesh(meshList[GEO_MAP3230], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(28, 0, -16);
	RenderMesh(meshList[GEO_MAP3236], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	RenderMesh(meshList[GEO_MAP3236], true);
	modelStack.PopMatrix();
	modelStack.PushMatrix();
	modelStack.Translate(-26, 0, 0);
	RenderMesh(meshList[GEO_MAP3236], true);
	modelStack.PopMatrix();

	Enemy();
	RenderBread();
	magazine();
	rendermagazine();
	Renderhealth();
	Healthkit();
	Bread();
	RenderArm();
	RenderUI();
}

//render specific objects here
void Scene_2::RenderMesh(Mesh * mesh, bool enableLight)
{

	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);


	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);


	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);

		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}


	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}
	mesh->Render(); //this line should only be called once 
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

}

void Scene_2::RenderUI()
{
	float ratio;
	float ratio2;
	float temphealth = health;
	ratio = (temphealth / 100);
	ratio2 = ratio * 9.7;
	string bread2 = to_string(bread);
	string ammo2 = to_string(ammo);


	if (Application::IsKeyPressed('6'))
	{
		consumables.cAdd(1);
		consumables.cDisplay();
		consumable = true;
	}
	if (Application::IsKeyPressed('7'))
	{
		consumables.cAdd(2);
		consumables.cDisplay();
		consumable = true;
	}
	if (Application::IsKeyPressed('8'))
	{
		consumables.cAdd(3);
		consumables.cDisplay();
		consumable = true;
	}
	if (Application::IsKeyPressed('9'))
	{
		consumables.cMove();
		consumables.cDisplay();
	}
	if (Application::IsKeyPressed('5'))
	{
		consumables.Consume();
		consumables.cDisplay();
	}
	if (consumable == false)
	{
	}
	else if (consumables.getConsumableData() == 1)
	{
		//render text UI here
		modelStack.PushMatrix();
		//scale, translate, rotate
		RenderTGAOnScreen(meshList[GEO_PISTOLPIC], Color(0, 1, 0), 3, 1, 1);
		modelStack.PopMatrix();
	}
	else if (consumables.getConsumableData() == 2)
	{
		//render text UI here
		modelStack.PushMatrix();
		//scale, translate, rotate
		RenderTGAOnScreen(meshList[GEO_SMGPIC], Color(0, 1, 0), 3, 1, 1);
		modelStack.PopMatrix();
	}
	else if (consumables.getConsumableData() == 3)
	{
		//render text UI here
		modelStack.PushMatrix();
		//scale, translate, rotate
		RenderTGAOnScreen(meshList[GEO_AK74PIC], Color(0, 1, 0), 3, 1, 1);
		modelStack.PopMatrix();
	}

	if (carded == true)
	{
		RenderTGAOnScreen(meshList[GEO_CARDUI], Color(1, 1, 1), 1, 48, 3), false;
	}


	//render text UI here
	RenderTextOnScreen(meshList[GEO_TEXT], ammo2, Color(1, 1, 1), 5, 5.7, 0.8), false;

	RenderTGAOnScreen(meshList[GEO_AMMO], Color(1, 1, 1), 1.3, 19, 3), false;

	RenderTextOnScreen(meshList[GEO_TEXT], bread2, Color(1, 1, 1), 5, 8.5, 0.8), false;

	RenderTGAOnScreen(meshList[GEO_BREADUI], Color(1, 1, 1), 1, 37, 3.5), false;

	RenderTGAOnScreen(meshList[GEO_HP4], Color(1, 1, 1), 2, 5, 1), false;
	RenderTGAOnScreen(meshList[GEO_HP5], Color(1, 1, 1), 2, -4.7 + ratio2, 1), false;
	RenderTGAOnScreen(meshList[GEO_HP6], Color(1, 1, 1), 2, 5, 1), false;
}

//void Scene_2::RenderUI()
//{
//	if (Application::IsKeyPressed('6'))
//	{
//			weapons.wAdd(1);
//			weapons.wDisplay();
//			weapon = true;
//	}
//	if (Application::IsKeyPressed('7'))
//	{
//		weapons.wAdd(2);
//		weapons.wDisplay();
//		weapon = true;
//	}
//	if (Application::IsKeyPressed('8'))
//	{
//		weapons.wAdd(3);
//		weapons.wDisplay();
//		weapon = true;
//	}
//	if (Application::IsKeyPressed('9'))
//	{
//		weapons.wMove();
//		weapons.wDisplay();
//	}
//	if (weapon == false)
//	{
//	}
//	else if (weapons.getWeaponData() == 1)
//	{
//		//render text UI here
//		modelStack.PushMatrix();
//		//scale, translate, rotate
//		RenderTGAOnScreen(meshList[GEO_PISTOLPIC], Color(0, 1, 0), 3, 1, 1, true);
//		modelStack.PopMatrix();
//	}
//	else if (weapons.getWeaponData() == 2)
//	{
//		//render text UI here
//		modelStack.PushMatrix();
//		//scale, translate, rotate
//		RenderTGAOnScreen(meshList[GEO_AK74PIC], Color(0, 1, 0), 3, 1, 1, true);
//		modelStack.PopMatrix();
//	}
//	else if (weapons.getWeaponData() == 3)
//	{
//		//render text UI here
//		modelStack.PushMatrix();
//		//scale, translate, rotate
//		RenderTGAOnScreen(meshList[GEO_SMGPIC], Color(0, 1, 0), 3, 1, 1, true);
//		modelStack.PopMatrix();
//	}
//}

void Scene_2::RenderCard()
{
	if (render_card1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(22, 0.2, -31);
		modelStack.Rotate(180, 1, 0, 0);
		RenderMesh(meshList[GEO_CARD], false);
		modelStack.PopMatrix();
	}

	if (render_card2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(22, 0.2, -29);
		modelStack.Rotate(180, 1, 0, 0);
		RenderMesh(meshList[GEO_CARD], false);
		modelStack.PopMatrix();
	}
}

void Scene_2::RenderDoor()
{
	if (door1_open == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(4, -1, 4);
		RenderMesh(meshList[GEO_DOOR], true);
		modelStack.PopMatrix();
	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, -1, 4);
		RenderMesh(meshList[GEO_DOOR], true);
		modelStack.PopMatrix();
	}

	if (door2_open == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(5.5, -1, 22);
		modelStack.Rotate(90, 0, 1, 0);
		RenderMesh(meshList[GEO_DOOR], true);
		modelStack.PopMatrix();
	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(5.5, -1, 25);
		modelStack.Rotate(90, 0, 1, 0);

		RenderMesh(meshList[GEO_DOOR], true);
		modelStack.PopMatrix();

		SceneManager::instance()->SetNextScene(2);
	}
}

void Scene_2::RenderSlowdownTrap1()
{
	modelStack.PushMatrix();
	modelStack.Translate(-22, 0, -19);
	RenderMesh(meshList[GEO_WIRE], true);
	modelStack.PopMatrix();

	/*if (trap_s1_end == false)
	{
	SlowdownTrapCheck1();
	}*/
}

void Scene_2::RenderSlowdownTrap2()
{
	modelStack.PushMatrix();
	modelStack.Translate(-22, 0, -21);
	RenderMesh(meshList[GEO_WIRE], true);
	modelStack.PopMatrix();

	if (trap_s2_end == false)
	{
		SlowdownTrapCheck2();
	}
}

void Scene_2::RenderSlowdownTrap3()
{
	modelStack.PushMatrix();
	modelStack.Translate(-22, 0, -23);
	RenderMesh(meshList[GEO_WIRE], true);
	modelStack.PopMatrix();

	if (trap_s3_end == false)
	{
		SlowdownTrapCheck3();
	}
}

void Scene_2::RenderInjureTrap()
{
	if (!mineblown)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-49, 0, 35);
		modelStack.Scale(0.2, 0.2, 0.2);
		modelStack.Rotate(90, 1, 0, 0);
		RenderMesh(meshList[GEO_MINE], false);
		modelStack.PopMatrix();
	}
	else if (mineblown)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-49, 0, 35);
		modelStack.Scale(0.2, 0.2, 0.2);
		modelStack.Rotate(90, 1, 0, 0);
		RenderMesh(meshList[GEO_EXPLODE], false);
		modelStack.PopMatrix();
	}

	if (trap2_end == false)
	{
		InjureTrapCheck();
	}
}

void Scene_2::RenderPoisonTrap()
{
	modelStack.PushMatrix();
	modelStack.Translate(-1, 0, 24);
	modelStack.Scale(0.05, 0.05, 0.05);
	RenderMesh(meshList[GEO_SPIKE], true);
	modelStack.PopMatrix();

	if (trap3_end == false)
	{
		PoisonTrapCheck();
	}
}

void Scene_2::RenderStopTrap()
{
	modelStack.PushMatrix();
	modelStack.Translate(-7, 0, 30);
	modelStack.Scale(0.05, 0.05, 0.05);
	RenderMesh(meshList[GEO_BEARTRAP], true);
	modelStack.PopMatrix();

	if (trap4_end == false)
	{
		StopTrapCheck();
	}
}

void Scene_2::Renderhealth()
{
	if (render_health == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-24, 0, 30);
		modelStack.Scale(0.1, 0.1, 0.1);
		RenderMesh(meshList[GEO_HEALTH], false);
		modelStack.PopMatrix();
	}
}

void Scene_2::rendermagazine()
{
	if (render_magazine == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-15, 0, 20);
		modelStack.Scale(0.1, 0.1, 0.1);
		RenderMesh(meshList[GEO_MAGAZINE], false);
		modelStack.PopMatrix();
	}
}

void Scene_2::RenderBread()
{
	if (render_bread == true)
	{
		modelStack.PushMatrix();
		modelStack.Scale(0.3, 0.3, 0.3);
		modelStack.Translate(-12, 0, -16);
		RenderMesh(meshList[GEO_BREAD], false);
		modelStack.PopMatrix();
	}
}

void Scene_2::Enemy()
{
	if (nemy == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-14, -1, 5);
		modelStack.Translate(enemy.x, 0, enemy.z);
		//modelStack.Rotate(5, 0, 0, 1);
		RenderMesh(meshList[GEO_ENEMY], false);
		modelStack.PopMatrix();
	}

	if (nemy2 == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(15, -1, 6);
		modelStack.Translate(enemy2.x, 0, enemy2.z);
		//modelStack.Rotate(5, 0, 0, 1);
		RenderMesh(meshList[GEO_ENEMY], false);
		modelStack.PopMatrix();
	}

	if (nemy3 == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-70, -1, -35);
		modelStack.Translate(enemy3.x, 0, enemy3.z);
		//modelStack.Rotate(5, 0, 0, 1);
		RenderMesh(meshList[GEO_ENEMY], false);
		modelStack.PopMatrix();
	}

	if (nemy4 == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(15, -1, 6);
		modelStack.Translate(enemy4.x, 0, enemy4.z);
		//modelStack.Rotate(5, 0, 0, 1);
		RenderMesh(meshList[GEO_ENEMY], false);
		modelStack.PopMatrix();
	}

	if (nemy5 == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(15, -1, 6);
		modelStack.Translate(enemy5.x, 0, enemy5.z);
		//modelStack.Rotate(5, 0, 0, 1);
		RenderMesh(meshList[GEO_ENEMY], false);
		modelStack.PopMatrix();
	}
}

void Scene_2::RenderModel()
{
	//render skybox here

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, 499.9, -500);
	RenderMesh(meshList[GEO_FRONT], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(-500, 499.9, 0);
	modelStack.Rotate(90, 0, 1, 0);
	RenderMesh(meshList[GEO_RIGHT], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, 499.9, 500);
	modelStack.Rotate(180, 0, 1, 0);
	RenderMesh(meshList[GEO_BACK], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(500, 499.9, 0);
	modelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_LEFT], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, 999.9, 0);
	modelStack.Rotate(90, 1, 0, 0);
	modelStack.Rotate(-90, 0, 0, 1);
	RenderMesh(meshList[GEO_TOP], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, -0.1, 0);
	modelStack.Rotate(-90, 1, 0, 0);
	RenderMesh(meshList[GEO_BOTTOM], true);
	modelStack.PopMatrix();
}

void Scene_2::RenderArm()
{
	glDisable(GL_DEPTH_TEST);
	Mtx44 Persp;
	Persp.SetToPerspective(60.f, 4.f / 3.f, 0.001f, 20.0f);
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(Persp);
	viewStack.PushMatrix();
	viewStack.LoadIdentity();
	modelStack.PushMatrix();
	modelStack.LoadIdentity();

	/*if (Application::IsKeyPressed(VK_LBUTTON))
	{*/
	modelStack.PushMatrix();

	modelStack.Translate(shoot.x, shoot.y, shoot.z);

	modelStack.Rotate(rotateGun, 1, 0, 0);
	modelStack.Scale(1.5, 1.5, 1.5);
	modelStack.PushMatrix();

	modelStack.Rotate(170, 0, 1, 0);
	RenderMesh(meshList[GEO_GUN], true);

	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(10, 0, -1, 0);
	RenderMesh(meshList[GEO_ARM], true);
	modelStack.PopMatrix();

	modelStack.PopMatrix();
	/*}
	else
	{
	modelStack.PushMatrix();

	modelStack.Translate(iron_sights.x, iron_sights.y, iron_sights.z);
	modelStack.Scale(1.5, 1.5, 1.5);
	modelStack.PushMatrix();
	modelStack.Rotate(170, 0, 1, 0);
	RenderMesh(meshList[GEO_GUN], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(10, 0, -1, 0);
	RenderMesh(meshList[GEO_ARM], false);
	modelStack.PopMatrix();

	modelStack.PopMatrix();
	}*/

	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
}

void Scene_2::RenderText(Mesh * mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 0.5f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void Scene_2::RenderTextOnScreen(Mesh * mesh, std::string text, Color color, float size, float x, float y)
{

	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 0.3f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);

	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
}

void Scene_2::RenderTGAOnScreen(Mesh* mesh, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	Mtx44 MVP, modelView, modelView_inverse_transpose, ortho;



	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);


	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}

	mesh->Render(); //this line should only be called once 

	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}



	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void Scene_2::Exit()
{
	// Cleanup VBO here
	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (meshList[i] != NULL)
		{
			delete meshList[i];
		}

		meshList[i] = NULL;

	}

	glDeleteProgram(m_programID);
}