#ifndef SCENE_2_H
#define SCENE_2_H

#include "Scene.h"
#include "MAIN_CAMERA.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"
#include "Light.h"
#include "Mtx44.h"
#include <string>
#include "Bullet.h"
#include "shooting.h"
#include "CRay.h"
#include "Box.h"
#include "Object.h"
#include "cLinkList.h"
//#include "wLinkList.h"

class Scene_2 : public Scene
{
	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,

		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,
		U_LIGHT0_TYPE,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,

		U_LIGHT1_POSITION,
		U_LIGHT1_COLOR,
		U_LIGHT1_POWER,
		U_LIGHT1_KC,
		U_LIGHT1_KL,
		U_LIGHT1_KQ,
		U_LIGHT1_TYPE,
		U_LIGHT1_SPOTDIRECTION,
		U_LIGHT1_COSCUTOFF,
		U_LIGHT1_COSINNER,
		U_LIGHT1_EXPONENT,

		U_LIGHT2_POSITION,
		U_LIGHT2_COLOR,
		U_LIGHT2_POWER,
		U_LIGHT2_KC,
		U_LIGHT2_KL,
		U_LIGHT2_KQ,
		U_LIGHT2_TYPE,
		U_LIGHT2_SPOTDIRECTION,
		U_LIGHT2_COSCUTOFF,
		U_LIGHT2_COSINNER,
		U_LIGHT2_EXPONENT,

		U_LIGHT3_POSITION,
		U_LIGHT3_COLOR,
		U_LIGHT3_POWER,
		U_LIGHT3_KC,
		U_LIGHT3_KL,
		U_LIGHT3_KQ,
		U_LIGHT3_TYPE,
		U_LIGHT3_SPOTDIRECTION,
		U_LIGHT3_COSCUTOFF,
		U_LIGHT3_COSINNER,
		U_LIGHT3_EXPONENT,

		U_LIGHT4_POSITION,
		U_LIGHT4_COLOR,
		U_LIGHT4_POWER,
		U_LIGHT4_KC,
		U_LIGHT4_KL,
		U_LIGHT4_KQ,
		U_LIGHT4_TYPE,
		U_LIGHT4_SPOTDIRECTION,
		U_LIGHT4_COSCUTOFF,
		U_LIGHT4_COSINNER,
		U_LIGHT4_EXPONENT,

		U_LIGHT5_POSITION,
		U_LIGHT5_COLOR,
		U_LIGHT5_POWER,
		U_LIGHT5_KC,
		U_LIGHT5_KL,
		U_LIGHT5_KQ,
		U_LIGHT5_TYPE,
		U_LIGHT5_SPOTDIRECTION,
		U_LIGHT5_COSCUTOFF,
		U_LIGHT5_COSINNER,
		U_LIGHT5_EXPONENT,

		U_LIGHT6_POSITION,
		U_LIGHT6_COLOR,
		U_LIGHT6_POWER,
		U_LIGHT6_KC,
		U_LIGHT6_KL,
		U_LIGHT6_KQ,
		U_LIGHT6_TYPE,
		U_LIGHT6_SPOTDIRECTION,
		U_LIGHT6_COSCUTOFF,
		U_LIGHT6_COSINNER,
		U_LIGHT6_EXPONENT,

		U_LIGHT7_POSITION,
		U_LIGHT7_COLOR,
		U_LIGHT7_POWER,
		U_LIGHT7_KC,
		U_LIGHT7_KL,
		U_LIGHT7_KQ,
		U_LIGHT7_TYPE,
		U_LIGHT7_SPOTDIRECTION,
		U_LIGHT7_COSCUTOFF,
		U_LIGHT7_COSINNER,
		U_LIGHT7_EXPONENT,

		U_LIGHT8_POSITION,
		U_LIGHT8_COLOR,
		U_LIGHT8_POWER,
		U_LIGHT8_KC,
		U_LIGHT8_KL,
		U_LIGHT8_KQ,
		U_LIGHT8_TYPE,
		U_LIGHT8_SPOTDIRECTION,
		U_LIGHT8_COSCUTOFF,
		U_LIGHT8_COSINNER,
		U_LIGHT8_EXPONENT,

		U_LIGHT9_POSITION,
		U_LIGHT9_COLOR,
		U_LIGHT9_POWER,
		U_LIGHT9_KC,
		U_LIGHT9_KL,
		U_LIGHT9_KQ,
		U_LIGHT9_TYPE,
		U_LIGHT9_SPOTDIRECTION,
		U_LIGHT9_COSCUTOFF,
		U_LIGHT9_COSINNER,
		U_LIGHT9_EXPONENT,

		U_LIGHTENABLED,
		U_NUMLIGHTS,
		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE,
		U_TEXT_ENABLED,
		U_TEXT_COLOR,
		U_TOTAL,
	};

	enum GEOMETRY_TYPE
	{
		GEO_AXES,
		GEO_LIGHTBALL,
		GEO_LIGHTBALL_CEILING,
		GEO_LEFT,
		GEO_RIGHT,
		GEO_TOP,
		GEO_BOTTOM,
		GEO_FRONT,
		GEO_BACK,
		GEO_QUAD,
		GEO_TEXT,

		//create geo types here
		GEO_CARD,
		GEO_DOOR,
		GEO_ARM,
		GEO_GUN,
		GEO_MAP326,
		GEO_MAP328,
		GEO_MAP3212,
		GEO_MAP3214,
		GEO_MAP3218,
		GEO_MAP3224,
		GEO_MAP3230,
		GEO_MAP3236,
		GEO_MAP3290,
		GEO_MAP32102,
		GEO_MAP366,
		GEO_MAP31218,
		GEO_WIRE,
		GEO_MINE,
		GEO_EXPLODE,
		GEO_SPIKE,
		GEO_BEARTRAP,
		GEO_PISTOLPIC,
		GEO_AK74PIC,
		GEO_SMGPIC,

		GEO_HP4,
		GEO_HP5,
		GEO_HP6,
		GEO_AMMO,
		GEO_ENEMY,
		GEO_OMAEWAMOSHINDEIRU,
		GEO_BREAD,
		GEO_HEALTH,
		GEO_BAR,
		GEO_MAGAZINE,
		GEO_BREADUI,
		GEO_CARDUI,

		//reminder to create above NUM_GEOMETRY
		NUM_GEOMETRY,
	};

	enum Game_state
	{
		GAME_1,
		GAME_2,
		GAME_3,
		GAME_4,
	};

public:
	Scene_2();
	~Scene_2();



	virtual void Init();
	virtual void Update(double dt);
	void mousePicking();
	virtual void Render();

	//define (virtual) obj specific functions here

	virtual void Exit();

private:
	unsigned m_vertexArrayID;
	Mesh* meshList[NUM_GEOMETRY];
	unsigned m_programID;
	unsigned m_parameters[U_TOTAL];
	Vector3 shoot;

	float rotateGun;
	bool mouse_debounce;

	bool activated = false;


	double timePassed;

	bool collided;
	bool collided2;
	bool collided3;
	bool collided4;
	bool collided5;
	int oof;
	int oof2;
	int oof3;
	int oof4;
	int oof5;

	bool nemy;
	bool nemy2;
	bool nemy3;
	bool nemy4;
	bool nemy5;

	Camera3 camera;
	MS modelStack, viewStack, projectionStack;
	Light light[10];
	Object object[21];
	std::vector<Projectile*> bullets;
	CRay raypicking;

	void RenderMesh(Mesh *mesh, bool enableLight);
	void RenderArm();
	void RenderModel();
	void RenderText(Mesh* mesh, std::string text, Color color);
	void RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y);
	void RenderTGAOnScreen(Mesh* mesh, Color color, float size, float x, float y);
	void BoundsCheck();
	void CardCheck();
	void DoorCheck();
	void RenderCard();
	void RenderDoor();
	void RenderUI();


	void Enemy();
	void Bread();
	void RenderBread();
	void Healthkit();
	void Renderhealth();
	void magazine();
	void rendermagazine();

	double FPS;
	std::string framerate;


	//define global bools and data here
	///////////////
	//Keys and Doors//
	///////////////
	bool door1_open;
	bool door2_open;

	bool collided_card1;
	bool collided_card2;
	bool collided_door1;
	bool collided_door2;
	bool carded;
	bool render_card1;
	bool render_card2;



	///////////////
	//Traps//
	///////////////////////
	void RenderSlowdownTrap1();
	void RenderSlowdownTrap2();
	void RenderSlowdownTrap3();
	void RenderInjureTrap();  //Injure takes off health immediately
	void RenderStopTrap();
	void RenderPoisonTrap(); //Debuff takes off health slowly

	void SlowdownTrapCheck1();
	void SlowdownTrapCheck2();
	void SlowdownTrapCheck3();
	void InjureTrapCheck();
	void StopTrapCheck();
	void PoisonTrapCheck();

	bool collided_s_trap1;
	bool collided_s_trap2;
	bool collided_s_trap3;
	bool collided_trap2;
	bool collided_trap3;
	bool collided_trap4;

	bool trap_s1_end;
	bool trap_s2_end;
	bool trap_s3_end;
	bool trap2_end;
	bool trap3_end;
	bool trap4_end;

	//trap effects
	void TrapSlowdown1();
	void TrapSlowdown2();
	void TrapSlowdown3();

	void TrapPoison();
	void TrapStop();
	void TrapInjure();

	int esc_s_trap1;
	int esc_s_trap2;
	int esc_s_trap3;
	int esc_trap2;
	int esc_trap3;
	int esc_trap4;

	bool inTrap_s1 = false;
	bool inTrap_s2 = false;
	bool inTrap_s3 = false;
	bool inTrap2 = false;
	bool inTrap3 = false;
	bool inTrap4 = false;

	bool trapped_s1 = false;
	bool trapped_s2 = false;
	bool trapped_s3 = false;
	bool trapped2 = false;
	bool trapped3 = false;
	bool trapped4 = false;

	bool mineblown;

	//Test health
	int health = 100;
	int debuff = 15;   //Time before effect ends
	int debuffTime = 30; //Time for looping effect
	int boom = 1;

	//Inventory
	cLinkList consumables;
	//wLinkList weapons;
	bool weapon = false;
	bool consumable = false;


	//Heath restore codes and ammo
	bool collided_healthstand;
	bool render_bread = true;
	bool render_health = true;
	bool render_magazine = true;

	int healthhealed;
	int breadhealed;
	int elapsedtime = 0;
	int elapsedtime2 = 0;
	int elapsedtime3 = 0;
	int elapsedtime4 = 0;
	int elapsedtime5 = 0;
	int bread = 1;
	int ammo = 1;
	Game_state gamestate = GAME_1;

	//Enemy AI
	void EnemyMoving(double dt);
	void EnemyMoving2(double dt);
	void EnemyMoving3(double dt);
	void EnemyMoving4(double dt);
	void EnemyMoving5(double dt);
	void EnemyHealth();
	void EnemyHealth2();
	void EnemyHealth3();
	void EnemyHealth4();
	void EnemyHealth5();

	Vector3 enemy = (0, 0, 0);
	Vector3 enemy2 = (0, 0, 0);
	Vector3 enemy3 = (0, 0, 0);
	Vector3 enemy4 = (0, 0, 0);
	Vector3 enemy5 = (0, 0, 0);

};

#endif   
