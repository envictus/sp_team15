#include "SP_SCENE_MENU.h"

using namespace std;
SP_SCENE_MENU::SP_SCENE_MENU()
{
}


SP_SCENE_MENU::~SP_SCENE_MENU()
{
}

void SP_SCENE_MENU::Init()
{
	mouse_debounce = false;
	start_select = true;

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glUniform1i(m_parameters[U_NUMLIGHTS], 1);

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);

	camera.Init(Vector3(0, 1, 0), Vector3(1, 1.5, 1), Vector3(0, 1, 0));

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 4000.f);
	projectionStack.LoadMatrix(projection);

	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights"); //in case you missed out practical 7																		// Get a handle for our "colorTexture" uniform
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");

	glUseProgram(m_programID);

	// Enable depth test
	glUniform1i(m_parameters[U_NUMLIGHTS], 1);
	glEnable(GL_DEPTH_TEST);


	// Make sure you pass uniform parameters after glUseProgram()


	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000, 1000, 1000);
	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	//init objects here
	meshList[GEO_QUAD] = MeshBuilder::GenerateQuad("menu", Color(1, 1, 1), 31.0f, 23.0f);
	meshList[GEO_QUAD]->textureID = LoadTGA("Image//menu.tga");
	meshList[GEO_SELECT] = MeshBuilder::GenerateQuad("menu", Color(1, 1, 1), 8.0f, 8.0f);
	meshList[GEO_SELECT]->textureID = LoadTGA("Image//selector.tga");
	meshList[GEO_TUTOR] = MeshBuilder::GenerateQuad("tutorial", Color(1, 1, 1), 31.0f, 23.0f);
	meshList[GEO_TUTOR]->textureID = LoadTGA("Image//Tutorial.tga");

	//init booleans here
	rotateAmt = 20;
	rotateAngle = 0.0f;

	//init object min max vertices here
}

void SP_SCENE_MENU::Update(double dt)
{
	//init update specific data here
	static const float LSPEED = 10.0f;

	if (Application::IsKeyPressed('1'))
	{
		glEnable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('2'))
	{
		glDisable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('3'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (Application::IsKeyPressed('4'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}


	//init update loops here
	rotateAngle += (float)(rotateAmt * dt);

	camera.Update(dt, object);
}

void SP_SCENE_MENU::Render()
{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);

	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	//call obj specific render functions here
	RenderMenu();

	modelStack.PushMatrix();
	RenderMesh(meshList[GEO_AXES], false);
	modelStack.PopMatrix();
	//render generic objects here
}

//render specific objects here
void SP_SCENE_MENU::RenderMenu()
{
	glDisable(GL_DEPTH_TEST);
	Mtx44 Persp;
	Persp.SetToPerspective(60.f, 4.f / 3.f, 0.001f, 20.0f);
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(Persp);
	viewStack.PushMatrix();
	viewStack.LoadIdentity();
	modelStack.PushMatrix();
	modelStack.LoadIdentity();

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -20);
	RenderMesh(meshList[GEO_QUAD], false);
	modelStack.PopMatrix();

		if (Application::IsKeyPressed('S'))
		{
			start_select = false;
		}
		else if (Application::IsKeyPressed('W'))
		{
			start_select = true;
		}

		if (start_select == false)
		{
			modelStack.PushMatrix();
			modelStack.Translate(2.5, -1, -19);
			RenderMesh(meshList[GEO_SELECT], false);
			modelStack.PopMatrix();
			if (Application::IsKeyPressed(VK_RETURN))
			{
				modelStack.PushMatrix();
				modelStack.Translate(0, 0, -20);
				RenderMesh(meshList[GEO_TUTOR], false);
				modelStack.PopMatrix();
			}
		}
		else
		{
			modelStack.PushMatrix();
			modelStack.Translate(-1.5, 1.5, -19);
			RenderMesh(meshList[GEO_SELECT], false);
			modelStack.PopMatrix();
			if (Application::IsKeyPressed(VK_RETURN))
			{
				SceneManager::instance()->SetNextScene(1);
			}
		}



	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
}

void SP_SCENE_MENU::RenderMesh(Mesh * mesh, bool enableLight)
{

	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);


	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);


	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}
	mesh->Render(); //this line should only be called once 
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

}

void SP_SCENE_MENU::Exit()
{
	// Cleanup VBO here
	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (meshList[i] != NULL)
		{
			delete meshList[i];
		}

		meshList[i] = NULL;

	}

	glDeleteProgram(m_programID);
}

