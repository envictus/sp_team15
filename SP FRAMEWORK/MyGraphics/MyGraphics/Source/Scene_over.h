#ifndef SCENE_OVER_H
#define SCENE_OVER_H

#include "Scene.h"
#include "SceneManager.h"
#include "LOOT_CAMERA.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"
#include "Light.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "Box.h"
#include "Object.h"

#include <string>
#include <iostream>

class Scene_over : public Scene
{
	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_LIGHT0_POSITION,
		U_NUMLIGHTS,
		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE,
		U_TEXT_ENABLED,
		U_TEXT_COLOR,
		U_TOTAL,
	};

	enum GEOMETRY_TYPE
	{
		GEO_AXES,
		GEO_LEFT,
		GEO_RIGHT,
		GEO_TOP,
		GEO_BOTTOM,
		GEO_FRONT,
		GEO_BACK,
		GEO_QUAD,
		GEO_TEXT,

		//create geo types here
		GEO_SELECT,
		GEO_TUTOR,


		//reminder to create above NUM_GEOMETRY
		NUM_GEOMETRY,
	};

public:
	Scene_over();
	~Scene_over();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();

	//define (virtual) obj specific functions here

	virtual void Exit();

private:
	unsigned m_vertexArrayID;
	Mesh* meshList[NUM_GEOMETRY];
	unsigned m_programID;
	unsigned m_parameters[U_TOTAL];

	float rotateAngle;
	float rotateAmt;
	bool mouse_debounce;
	bool start_select;

	CameraLoot camera;
	MS modelStack, viewStack, projectionStack;
	Light light[1];
	Object object[1];

	void RenderMenu();

	void RenderMesh(Mesh *mesh, bool enableLight);

};

#endif   
