#ifndef SHOOTING_H
#define SHOOTING_H
#include "Vertex.h"

class Shooting
{
public:
	Shooting();
	Shooting(Vector3 position, Vector3 target);
	~Shooting();
	void move();
	Vector3 position;
	Vector3 target;
	Vector3 view;
};
#endif