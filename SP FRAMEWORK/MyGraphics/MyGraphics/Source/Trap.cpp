#include "SP_SCENE_1.h"
#include "SP_SCENE_2.h"
#include "GL\glew.h"

#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "Box.h"
#include "Object.h"

#include <iostream>

using namespace std;



void SceneText::SlowdownTrapCheck1()
{
	Box trap1 = Box(Vector3(-22, 0, -19), 1);
	collided_s_trap1 = trap1.isPointInBox(camera.position, trap1);

	if (collided_s_trap1 && !inTrap_s1)
	{
		if (esc_s_trap1 != 0)
		{
			cout << "Gotcha" << endl;
			trapped_s1 = true;


			if (Application::IsKeyPressed('E'))
			{
				if (esc_s_trap1 != 0)
				{
					esc_s_trap1--;
				}
				else if (esc_s_trap1 == 0)
				{
					trapped_s1 = false;
					esc_s_trap1 = true;
				}
			}
		}
		else
		{
			trapped_s1 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped_s1 = false;
		//esc_trap = 50;                             
	}
	TrapSlowdown1();
}

void SceneText::SlowdownTrapCheck2()
{
	Box traps2 = Box(Vector3(-22, 0, -21), 1);
	collided_s_trap2 = traps2.isPointInBox(camera.position, traps2);

	if (collided_s_trap2 && !inTrap_s2)
	{
		if (esc_s_trap2 != 0)
		{
			cout << "Gotcha" << endl;
			trapped_s2 = true;


			if (Application::IsKeyPressed('E'))
			{
				if (esc_s_trap2 != 0)
				{
					esc_s_trap2--;
				}
				else if (esc_s_trap2 == 0)
				{
					trapped_s2 = false;
					esc_s_trap2 = true;
				}
			}
		}
		else
		{
			trapped_s2 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped_s2 = false;
		//esc_trap = 50;                             
	}
	TrapSlowdown2();
}

void SceneText::SlowdownTrapCheck3()
{
	Box traps3 = Box(Vector3(-22, 0, -23), 1);
	collided_s_trap3 = traps3.isPointInBox(camera.position, traps3);

	if (collided_s_trap3 && !inTrap_s3)
	{
		if (esc_s_trap3 != 0)
		{
			cout << "Gotcha" << endl;
			trapped_s3 = true;


			if (Application::IsKeyPressed('E'))
			{
				if (esc_s_trap3 != 0)
				{
					esc_s_trap3--;
				}
				else if (esc_s_trap3 == 0)
				{
					trapped_s3 = false;
					esc_s_trap3 = true;
				}
			}
		}
		else
		{
			trapped_s3 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped_s3 = false;
		//esc_trap = 50;                             
	}
	TrapSlowdown3();
}

void SceneText::InjureTrapCheck()
{
	Box trap2 = Box(Vector3(-49, 0, 35), 1);
	collided_trap2 = trap2.isPointInBox(camera.position, trap2);


	if (collided_trap2 && !inTrap2)
	{
		if (esc_trap2 != 0)
		{
			cout << "Gotcha" << endl;
			trapped2 = true;

			if (Application::IsKeyPressed('E'))
			{
				if (esc_trap2 != 0)
				{
					esc_trap2--;
				}
				else if (esc_trap2 == 0)
				{
					trapped2 = false;
					trap2_end = true;
				}
			}
		}
		else
		{
			trapped2 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped2 = false;
		//esc_trap = 50;
	}
	TrapInjure();
}



void SceneText::PoisonTrapCheck()
{
	Box trap3 = Box(Vector3(1, 0, 23), 1);
	collided_trap3 = trap3.isPointInBox(camera.position, trap3);


	if (collided_trap3 && !inTrap3)
	{
		if (esc_trap3 != 0)
		{
			cout << "Gotcha" << endl;
			trapped3 = true;

			if (Application::IsKeyPressed('E'))
			{
				if (esc_trap3 != 0)
				{
					esc_trap3--;
				}
				else if (esc_trap3 == 0)
				{
					trapped3 = false;
					trap3_end = true;
				}
			}
		}
		else
		{
			trapped3 = false;
			//esc_trap = 50;
		}

	}
	else
	{
		trapped3 = false;
		//esc_trap = 50;
	}
	TrapPoison();
	//TrapStop();
	//TrapInjure();
}

void SceneText::StopTrapCheck()
{
	Box trap4 = Box(Vector3(-7, 0, 30), 1);
	collided_trap4 = trap4.isPointInBox(camera.position, trap4);


	if (collided_trap4 && !inTrap4)
	{
		if (esc_trap4 != 0)
		{
			cout << "Gotcha" << endl;
			trapped4 = true;

			if (Application::IsKeyPressed('E'))
			{
				if (esc_trap4 != 0)
				{
					esc_trap4--;
				}
				else if (esc_trap4 == 0)
				{
					trapped4 = false;
					trap4_end = true;
				}
			}
		}
		else
		{
			trapped4 = false;
			//esc_trap = 50;
		}

	}
	else
	{
		trapped4 = false;
		//esc_trap = 50;
	}

	TrapStop();
	//TrapInjure();
}

void SceneText::TrapPoison()
{
	bool minusHealth = false;

	if (trapped3)
	{

		if (debuff != 0)
		{
			if (debuffTime == 0)
			{
				minusHealth = true;
				debuff--;

			}
			else
			{
				debuffTime--;
				minusHealth = false;
			}
		}
		else if (debuff == 0)
		{
			minusHealth = false;
		}
	}
	else  //reset timer
	{
		debuffTime = 30;
		debuff = 15;
	}

	if (minusHealth)
	{
		health--;
		debuffTime = 30;
	}

}

void SceneText::TrapSlowdown1()
{
	if (trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		camera.normalSpeed = false;
		camera.slowdown = true;
	}
	else if (!trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		//trap1_end = true;
		camera.normalSpeed = true;
		camera.slowdown = false;
	}
}

void SceneText::TrapSlowdown2()
{
	if (trapped_s2 && !trapped_s1 && !trapped_s3)
	{
		camera.normalSpeed = false;
		camera.slowdown = true;
	}
	else if (!trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		//trap1_end = true;
		camera.normalSpeed = true;
		camera.slowdown = false;
	}
}

void SceneText::TrapSlowdown3()
{

	if (!trapped_s1 && !trapped_s2 && trapped_s3)
	{
		camera.normalSpeed = false;
		camera.slowdown = true;
	}
	else if (!trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		//trap1_end = true;
		camera.normalSpeed = true;
		camera.slowdown = false;
	}

}


void SceneText::TrapStop()
{
	if (trapped4)
	{
		camera.move = false;
	}
	else if (!trapped4)
	{
		camera.move = true;
	}
}

void SceneText::TrapInjure()
{

	if (trapped2)
	{
		if (boom != 0)
		{
			health = health - 10;
			mineblown = true;
			boom--;
		}
	}
	else
	{
		boom = 1;
	}
}

void Scene_2::SlowdownTrapCheck1()
{
	Box trap1 = Box(Vector3(-22, 0, -19), 1);
	collided_s_trap1 = trap1.isPointInBox(camera.position, trap1);

	if (collided_s_trap1 && !inTrap_s1)
	{
		if (esc_s_trap1 != 0)
		{
			cout << "Gotcha" << endl;
			trapped_s1 = true;


			if (Application::IsKeyPressed('E'))
			{
				if (esc_s_trap1 != 0)
				{
					esc_s_trap1--;
				}
				else if (esc_s_trap1 == 0)
				{
					trapped_s1 = false;
					esc_s_trap1 = true;
				}
			}
		}
		else
		{
			trapped_s1 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped_s1 = false;
		//esc_trap = 50;                             
	}
	TrapSlowdown1();
}

void Scene_2::SlowdownTrapCheck2()
{
	Box traps2 = Box(Vector3(-22, 0, -21), 1);
	collided_s_trap2 = traps2.isPointInBox(camera.position, traps2);

	if (collided_s_trap2 && !inTrap_s2)
	{
		if (esc_s_trap2 != 0)
		{
			cout << "Gotcha" << endl;
			trapped_s2 = true;


			if (Application::IsKeyPressed('E'))
			{
				if (esc_s_trap2 != 0)
				{
					esc_s_trap2--;
				}
				else if (esc_s_trap2 == 0)
				{
					trapped_s2 = false;
					esc_s_trap2 = true;
				}
			}
		}
		else
		{
			trapped_s2 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped_s2 = false;
		//esc_trap = 50;                             
	}
	TrapSlowdown2();
}

void Scene_2::SlowdownTrapCheck3()
{
	Box traps3 = Box(Vector3(-22, 0, -23), 1);
	collided_s_trap3 = traps3.isPointInBox(camera.position, traps3);

	if (collided_s_trap3 && !inTrap_s3)
	{
		if (esc_s_trap3 != 0)
		{
			cout << "Gotcha" << endl;
			trapped_s3 = true;


			if (Application::IsKeyPressed('E'))
			{
				if (esc_s_trap3 != 0)
				{
					esc_s_trap3--;
				}
				else if (esc_s_trap3 == 0)
				{
					trapped_s3 = false;
					esc_s_trap3 = true;
				}
			}
		}
		else
		{
			trapped_s3 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped_s3 = false;
		//esc_trap = 50;                             
	}
	TrapSlowdown3();
}

void Scene_2::InjureTrapCheck()
{
	Box trap2 = Box(Vector3(-49, 0, 35), 1);
	collided_trap2 = trap2.isPointInBox(camera.position, trap2);


	if (collided_trap2 && !inTrap2)
	{
		if (esc_trap2 != 0)
		{
			cout << "Gotcha" << endl;
			trapped2 = true;

			if (Application::IsKeyPressed('E'))
			{
				if (esc_trap2 != 0)
				{
					esc_trap2--;
				}
				else if (esc_trap2 == 0)
				{
					trapped2 = false;
					trap2_end = true;
				}
			}
		}
		else
		{
			trapped2 = false;
			//esc_trap = 50;
		}
	}
	else
	{
		trapped2 = false;
		//esc_trap = 50;
	}
	TrapInjure();
}



void Scene_2::PoisonTrapCheck()
{
	Box trap3 = Box(Vector3(-1, 0, 24), 1);
	collided_trap3 = trap3.isPointInBox(camera.position, trap3);


	if (collided_trap3 && !inTrap3)
	{
		if (esc_trap3 != 0)
		{
			cout << "Gotcha" << endl;
			trapped3 = true;

			if (Application::IsKeyPressed('E'))
			{
				if (esc_trap3 != 0)
				{
					esc_trap3--;
				}
				else if (esc_trap3 == 0)
				{
					trapped3 = false;
					trap3_end = true;
				}
			}
		}
		else
		{
			trapped3 = false;
			//esc_trap = 50;
		}

	}
	else
	{
		trapped3 = false;
		//esc_trap = 50;
	}
	TrapPoison();
	//TrapStop();
	//TrapInjure();
}

void Scene_2::StopTrapCheck()
{
	Box trap4 = Box(Vector3(-7, 0, 30), 1);
	collided_trap4 = trap4.isPointInBox(camera.position, trap4);


	if (collided_trap4 && !inTrap4)
	{
		if (esc_trap4 != 0)
		{
			cout << "Gotcha" << endl;
			trapped4 = true;

			if (Application::IsKeyPressed('E'))
			{
				if (esc_trap4 != 0)
				{
					esc_trap4--;
				}
				else if (esc_trap4 == 0)
				{
					trapped4 = false;
					trap4_end = true;
				}
			}
		}
		else
		{
			trapped4 = false;
			//esc_trap = 50;
		}

	}
	else
	{
		trapped4 = false;
		//esc_trap = 50;
	}

	TrapStop();
	//TrapInjure();
}

void Scene_2::TrapPoison()
{
	bool minusHealth = false;

	if (trapped3)
	{

		if (debuff != 0)
		{
			if (debuffTime == 0)
			{
				minusHealth = true;
				debuff--;

			}
			else
			{
				debuffTime--;
				minusHealth = false;
			}
		}
		else if (debuff == 0)
		{
			minusHealth = false;
		}
	}
	else  //reset timer
	{
		debuffTime = 30;
		debuff = 15;
	}

	if (minusHealth)
	{
		health--;
		debuffTime = 30;
	}

}

void Scene_2::TrapSlowdown1()
{
	if (trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		camera.normalSpeed = false;
		camera.slowdown = true;
	}
	else if (!trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		//trap1_end = true;
		camera.normalSpeed = true;
		camera.slowdown = false;
	}
}

void Scene_2::TrapSlowdown2()
{
	if (trapped_s2 && !trapped_s1 && !trapped_s3)
	{
		camera.normalSpeed = false;
		camera.slowdown = true;
	}
	else if (!trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		//trap1_end = true;
		camera.normalSpeed = true;
		camera.slowdown = false;
	}
}

void Scene_2::TrapSlowdown3()
{

	if (!trapped_s1 && !trapped_s2 && trapped_s3)
	{
		camera.normalSpeed = false;
		camera.slowdown = true;
	}
	else if (!trapped_s1 && !trapped_s2 && !trapped_s3)
	{
		//trap1_end = true;
		camera.normalSpeed = true;
		camera.slowdown = false;
	}

}


void Scene_2::TrapStop()
{
	if (trapped4)
	{
		camera.move = false;
	}
	else if (!trapped4)
	{
		camera.move = true;
	}
}

void Scene_2::TrapInjure()
{

	if (trapped2)
	{
		if (boom != 0)
		{
			health = health - 10;
			mineblown = true;
			boom--;
		}
	}
	else
	{
		boom = 1;
	}
}

