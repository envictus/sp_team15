#include "Utility.h"

Position operator * (const Mtx44& lhs, const Position& rhs)
{
	float b[4];

	for (int i = 0; i < 4; i++)
	{
		b[i] = lhs.a[0 * 4 + i] * rhs.x + lhs.a[1 * 4 + i] * rhs.y +
			lhs.a[2 * 4 + i] * rhs.z + lhs.a[3 * 4 + i] * 1;
	}

	return Position(b[0], b[1], b[2]);
}

Vector4 operator * (const Vector4 &lhs, const Mtx44 &rhs)
{
	float output[4];
	int offset = 0;


	for (int x = 0; x < 4; x++)
	{
		offset = x *4;

		output[x] = 
			(lhs.x *  rhs.a[0 + offset]) + 
			(lhs.y * rhs.a[1 + offset]) + 
			(lhs.z * rhs.a[2 + offset]) + 
			(lhs.w * rhs.a[3 + offset]);
	}

	Vector4 vec4;
	vec4.x = output[0];
	vec4.y = output[1];
	vec4.z = output[2];
	vec4.w = output[3];

	return vec4;
}

Vector4 operator * (const Mtx44 &lhs, const Vector4 &rhs)
{
	Vector4 vec4;

	float output[4];

	int offset = 0;

	for (int i = 0; i < 4; i++)
	{
		output[i] = 
			lhs.a[0 + offset] * rhs.x +
			lhs.a[1 + offset] * rhs.y +
			lhs.a[2 + offset] * rhs.z +
			lhs.a[3 + offset] * rhs.w;

		offset += 4;
	}

	vec4.Set(output[0], output[1], output[2], output[3]);

	return vec4;
}