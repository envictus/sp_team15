#ifndef UTILITY_H
#define UTILITY_H

#include "Vertex.h"
#include "Mtx44.h"
#include "Vector4.h"

Position operator * (const Mtx44& lhs, const Position& rhs);
Vector4 operator * (const Vector4 &lhs, const Mtx44 &rhs);
Vector4 operator * (const Mtx44 &lhs, const Vector4 &rhs);
#endif // !UTILITY_H
