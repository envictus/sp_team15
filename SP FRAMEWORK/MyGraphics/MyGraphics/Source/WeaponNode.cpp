#include "WeaponNode.h"
#include <iostream>
#include <string>

using namespace std;

WeaponNode::WeaponNode()
{
	wData = NULL;
	damage = NULL;
}

WeaponNode::WeaponNode(int d, int h)
{
	wData = d;
	damage = h;
	wNext = NULL;
	wPrev = NULL;
}

WeaponNode::~WeaponNode()
{
}

int WeaponNode::wGetData()
{
	return wData;
}

void WeaponNode::wSetData(int d)
{
	wData = d;
}

int WeaponNode::getDamage()
{
	return damage;
}

void WeaponNode::setDamage(int dmg)
{
	damage = dmg;
}
