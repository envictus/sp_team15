#pragma once
class WeaponNode
{
private:
	int wData;
	int damage;
public:
	WeaponNode();
	WeaponNode(int, int);
	~WeaponNode();
	WeaponNode* wNext;
	WeaponNode* wPrev;

	int wGetData();
	void wSetData(int);

	int getDamage();
	void setDamage(int);
};

