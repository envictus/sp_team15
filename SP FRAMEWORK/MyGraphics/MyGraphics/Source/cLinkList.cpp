#include "cLinkList.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

cLinkList::cLinkList()
{
	breadCount = 0;
	healthpackCount = 0;
	chickenCount = 0;
	cCounter = 0;
	cLast = NULL;
	cIndicator = NULL;
}

cLinkList::~cLinkList()
{
}

void cLinkList::cAdd(int x)
{
	if (x == 1)
	{
		if (breadCount < 1)
		{
			AddConsumable(1, 10);
			breadCount++;
		}
		else
		{
			breadCount++;
		}
	}
	if (x == 2)
	{
		if (healthpackCount < 1)
		{
			AddConsumable(2, 30);
			healthpackCount++;
		}
		else
		{
			healthpackCount++;
		}
	}
	if (x == 3)
	{
		if (chickenCount < 1)
		{
			AddConsumable(3, 20);
			chickenCount++;
		}
		else
		{
			chickenCount++;
		}
	}
}

void cLinkList::AddConsumable(int d, int h)
{
	ConsumableNode* current;
	current = new ConsumableNode(d, h);
	if (cLast == NULL)
	{
		cLast = current;
		current->cNext = cLast;
		current->cPrev = cLast;
	}
	else
	{
		current->cNext = cLast->cNext;
		cLast->cNext = current;
		current->cPrev = cLast;
		cLast = current;
	}
}

void cLinkList::Consume()
{
	if (cLast == NULL)
	{
	}
	else if (cIndicator->getData() == 1)
	{
		if (breadCount < 2)
		{
			ConsumableNode* del, *current;
			del = cIndicator;
			int consume = del->getHPGain();
			/* If List has only one element*/
			if (cLast->cNext == cLast)
			{
				del = cLast;
				cLast = NULL;
				breadCount--;
				cout << "You have eaten bread and gained " << cIndicator->getHPGain() << " health. You have " << breadCount << " bread left." << endl;
				delete del;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			if (del == cLast->cNext)  /*cFirst Element Deletion*/
			{
				cLast->cNext = del->cNext;
				breadCount--;
				cout << "You have eaten bread and gained " << cIndicator->getHPGain() << " health. You have " << breadCount << " bread left." << endl;
				delete del;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			/*Deletion of cLast element*/
			if (del == cLast)
			{
				current = cIndicator->cPrev;
				cIndicator = cLast->cNext;
				breadCount--;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				cout << "You have eaten bread and gained " << cIndicator->getHPGain() << " health. You have " << breadCount << " bread left." << endl;
				delete del;
				cLast = current;
				current->cNext = cIndicator;
				cIndicator->cPrev = current;
				return;
			}
			/*Deletion of Element in between*/
			else
			{
				current = cIndicator->cPrev;
				cIndicator = cIndicator->cNext;
				breadCount--;
				cout << "You have eaten bread and gained " << cIndicator->getHPGain() << " health. You have " << breadCount << " bread left." << endl;
				delete del;
				current->cNext = cIndicator;
				cIndicator->cPrev = current;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			cout << "Item " << consume << " not found in the list" << endl;
		}
		else
		{
			breadCount--;
			cout << "You have eaten bread and gained " << cIndicator->getHPGain() << " health. You have " << breadCount << " bread left." << endl;
		}
	}
	else if (cIndicator->getData() == 2)
	{
		if (healthpackCount < 2)
		{
			ConsumableNode* del, *current;
			del = cIndicator;
			int consume = del->getHPGain();
			/* If List has only one element*/
			if (cLast->cNext == cLast)
			{
				del = cLast;
				cLast = NULL;
				healthpackCount--;
				cout << "You have eaten healthpack and gained " << cIndicator->getHPGain() << " health. You have " << healthpackCount << " bread left." << endl;
				delete del;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			if (del == cLast->cNext)  /*cFirst Element Deletion*/
			{
				cLast->cNext = del->cNext;
				healthpackCount--;
				cout << "You have eaten healthpack and gained " << cIndicator->getHPGain() << " health. You have " << healthpackCount << " healthpacks left." << endl;
				delete del;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			/*Deletion of cLast element*/
			if (del == cLast)
			{
				current = cIndicator->cPrev;
				cIndicator = cLast->cNext;
				healthpackCount--;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				cout << "You have eaten bread and gained " << cIndicator->getHPGain() << " health. You have " << healthpackCount << " healthpacks left." << endl;
				delete del;
				cLast = current;
				current->cNext = cIndicator;
				cIndicator->cPrev = current;
				return;
			}
			/*Deletion of Element in between*/
			else
			{
				current = cIndicator->cPrev;
				cIndicator = cIndicator->cNext;
				healthpackCount--;
				cout << "You have eaten healthpack and gained " << cIndicator->getHPGain() << " health. You have " << healthpackCount << " healthpacks left." << endl;
				delete del;
				current->cNext = cIndicator;
				cIndicator->cPrev = current;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			cout << "Item " << consume << " not found in the list" << endl;
		}
		else
		{
			healthpackCount--;
			cout << "You have eaten healthpack and gained " << cIndicator->getHPGain() << " health. You have " << healthpackCount << " healthpacks left." << endl;
		}
	}
	else if (cIndicator->getData() == 3)
	{
		if (chickenCount < 2)
		{
			ConsumableNode* del, *current;
			del = cIndicator;
			int consume = del->getHPGain();
			/* If List has only one element*/
			if (cLast->cNext == cLast)
			{
				del = cLast;
				cLast = NULL;
				chickenCount--;
				cout << "You have eaten chicken and gained " << cIndicator->getHPGain() << " health. You have " << chickenCount << " chicken left." << endl;
				delete del;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			if (del == cLast->cNext)  /*cFirst Element Deletion*/
			{
				cLast->cNext = del->cNext;
				chickenCount--;
				cout << "You have eaten chicken and gained " << cIndicator->getHPGain() << " health. You have " << chickenCount << " chicken left." << endl;
				delete del;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			/*Deletion of cLast element*/
			if (del == cLast)
			{
				current = cIndicator->cPrev;
				cIndicator = cLast->cNext;
				chickenCount--;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				cout << "You have eaten chicken and gained " << cIndicator->getHPGain() << " health. You have " << chickenCount << " chicken left." << endl;
				delete del;
				cLast = current;
				current->cNext = cIndicator;
				cIndicator->cPrev = current;
				return;
			}
			/*Deletion of Element in between*/
			else
			{
				current = cIndicator->cPrev;
				cIndicator = cIndicator->cNext;
				chickenCount--;
				cout << "You have eaten chicken and gained " << cIndicator->getHPGain() << " health. You have " << chickenCount << " chicken left." << endl;
				delete del;
				current->cNext = cIndicator;
				cIndicator->cPrev = current;
				cout << "Item " << consume;
				cout << " deleted from the list" << endl;
				return;
			}
			cout << "Item " << consume << " not found in the list" << endl;
		}
		else
		{
			chickenCount--;
			cout << "You have eaten chicken and gained " << cIndicator->getHPGain() << " health. You have " << chickenCount << " bread left." << endl;
		}
	}
}

void cLinkList::cMove()
{
	if (cLast == NULL)
	{
	}
	else if (cIndicator->cNext == cLast->cNext)
	{
		cIndicator = cLast->cNext;
		cCounter = 0;
	}
	else
	{
		cIndicator = cIndicator->cNext;
		cCounter++;
	}
}

void cLinkList::cDisplay()
{
	if (cLast == NULL)
	{
		return;
	}
	cIndicator = cLast->cNext;
	for (int i = 0; i < cCounter; i++)
	{
		cIndicator = cIndicator->cNext;
	}
	cout << cIndicator->getHPGain() << endl;
	if (cIndicator->getData() == 1)
	{
		cout << breadCount << endl;
	}
	else if (cIndicator->getData() == 2)
	{
		cout << healthpackCount << endl;
	}
	else if (cIndicator->getData() == 3)
	{
		cout << chickenCount << endl;
	}
}

void cLinkList::cDisplay_list()
{
	ConsumableNode* current;
	if (cLast == NULL)
	{
		cout << "List is empty, nothing to cDisplay" << endl;
		return;
	}
	current = cLast->cNext;
	cout << "Circular Link List: " << endl;
	while (current != cLast)
	{
		cout << current->getHPGain() << "->";
		current = current->cNext;
	}
	cout << current->getHPGain() << endl;
}

int cLinkList::getBreadCount()
{
	return breadCount;
}

int cLinkList::getHealthpackCount()
{
	return healthpackCount;
}

int cLinkList::getChickenCount()
{
	return chickenCount;
}

int cLinkList::getConsumableData()
{
	return cIndicator->getData();
}
