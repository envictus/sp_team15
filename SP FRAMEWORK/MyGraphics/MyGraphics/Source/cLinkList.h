#pragma once
#include "ConsumableNode.h"

class cLinkList : public ConsumableNode
{
private:
	int breadCount;
	int healthpackCount;
	int chickenCount;
	int cCounter;
	ConsumableNode* cFirst;
	ConsumableNode* cLast;
	ConsumableNode* cIndicator;
public:
	cLinkList();
	~cLinkList();

	void cAdd(int);
	void AddConsumable(int, int);
	void Consume();
	void cMove();
	void cDisplay();
	void cDisplay_list();

	int getBreadCount();
	int getHealthpackCount();
	int getChickenCount();

	int getConsumableData();
};
