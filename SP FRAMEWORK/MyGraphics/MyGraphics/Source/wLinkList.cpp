#include "wLinkList.h"
#include <iostream>

using namespace std;

wLinkList::wLinkList()
{
	wCounter = NULL;
	wLast = NULL;
	wIndicator = NULL;
}

wLinkList::~wLinkList()
{
}

void wLinkList::wAdd(int x)
{
	if (x == 1)
	{
		AddWeapon(1, 10);
	}
	if (x == 2)
	{
		AddWeapon(2, 20);
	}
	if (x == 3)
	{
		AddWeapon(3, 30);
	}
}

void wLinkList::AddWeapon(int d, int h)
{
	WeaponNode* current;
	current = new WeaponNode(d, h);
	if (wLast == NULL)
	{
		wLast = current;
		current->wNext = wLast;
		current->wPrev = wLast;
	}
	else
	{
		current->wNext = wLast->wNext;
		wLast->wNext = current;
		current->wPrev = wLast;
		wLast = current;
	}
}

void wLinkList::wMove()
{
	if (wLast == NULL)
	{
	}
	else if (wIndicator->wNext == wLast->wNext)
	{
		wIndicator = wLast->wNext;
		wCounter = 0;
	}
	else
	{
		wIndicator = wIndicator->wNext;
		wCounter++;
	}
}

void wLinkList::wDisplay()
{
	if (wLast == NULL)
	{
		return;
	}
	wIndicator = wLast->wNext;
	for (int i = 0; i < wCounter; i++)
	{
		wIndicator = wIndicator->wNext;
	}
	wIndicator->wGetData();
}

void wLinkList::wDisplay_list()
{
	WeaponNode* current;
	if (wLast == NULL)
	{
		cout << "List is empty, nothing to wDisplay" << endl;
		return;
	}
	current = wLast->wNext;
	cout << "Circular Link List: " << endl;
	while (current != wLast)
	{
		cout << current->getDamage() << "->";
		current = current->wNext;
	}
	cout << current->getDamage() << endl;
}

int wLinkList::getWeaponData()
{
	return wIndicator->wGetData();
}

int wLinkList::wGetCounter()
{
	return wCounter;
}
