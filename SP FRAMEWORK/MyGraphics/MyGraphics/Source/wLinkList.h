#include "WeaponNode.h"

class wLinkList : public WeaponNode
{
private:
	int wCounter;
	WeaponNode* wFirst;
	WeaponNode* wLast;
	WeaponNode* wIndicator;
public:
	wLinkList();
	~wLinkList();

	void wAdd(int);
	void AddWeapon(int, int);
	void wMove();
	void wDisplay();
	void wDisplay_list();
	int getWeaponData();
	int wGetCounter();
};